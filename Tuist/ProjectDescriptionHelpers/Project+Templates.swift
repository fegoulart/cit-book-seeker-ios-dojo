import ProjectDescription
import ProjectDescriptionHelpers

extension Project {
    static var bundleId: String { "com.goulart." }
    /// Helper function to create the Project
    public static func app(
        name: String,
        platform: Platform,
        deploymentTarget: DeploymentTarget,
        additionalTargets: [String],
        packages: [Package] = [],
        appLibDependencies: [TargetDependency] = [],
        uiTestLibDependencies: [TargetDependency] = [],
        unitTestLibDependencies: [TargetDependency] = [],
        scripts: [TargetScript] = []
    ) -> Project {

        var targets = makeAppTargets(
            name: name,
            platform: platform,
            deploymentTarget: deploymentTarget,
            dependencies: additionalTargets.map { TargetDependency.target(name: $0) },
            appLibDependencies: appLibDependencies,
            uiTestLibDependencies: uiTestLibDependencies,
            unitTestLibDependencies: unitTestLibDependencies,
            scripts: scripts
        )

        targets += additionalTargets.flatMap {
            makeFrameworkTargets(
                name: $0,
                platform: platform,
                deploymentTarget: deploymentTarget
            )
        }

        let schemes = makeAppSchemes(name: name)

        return Project(
            name: name,
            organizationName: "Goulart",
            packages: packages,
            targets: targets,
            schemes: schemes
        )
    }

    // MARK: - Private

    /// Helper function to create a framework target and an associated unit test target
    private static func makeFrameworkTargets(
        name: String,
        platform: Platform,
        deploymentTarget: DeploymentTarget
    ) -> [Target] {

        let sources = Target(
            name: name,
            platform: platform,
            product: .framework,
            bundleId: "\(bundleId)\(name)",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["Targets/\(name)/Sources/**"],
            resources: [],
            dependencies: []
        )

        let tests = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(bundleId)\(name)Tests",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["Targets/\(name)/Tests/**"],
            resources: [],
            dependencies: [.target(name: name)]
        )

        return [sources, tests]
    }

    /// Helper function to create the application target and the unit test target.
    private static func makeAppTargets(
        name: String,
        platform: Platform,
        deploymentTarget: DeploymentTarget,
        dependencies: [TargetDependency],
        appLibDependencies: [TargetDependency] = [],
        uiTestLibDependencies: [TargetDependency] = [],
        unitTestLibDependencies: [TargetDependency] = [],
        scripts: [TargetScript] = []
    ) -> [Target] {

        let appDependencies = dependencies + appLibDependencies

        let platform: Platform = platform

        let mainTarget = Target(
            name: name,
            platform: platform,
            product: .app,
            bundleId: "\(bundleId)\(name)",
            deploymentTarget: deploymentTarget,
            infoPlist: .file(path: "BookSeeker.plist"),
            sources: ["Sources/\(name)/**"],
            resources: ["Resources/**"],
            scripts: scripts,
            dependencies: appDependencies
        )

        let testTarget = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(bundleId)\(name)Tests",
            deploymentTarget: deploymentTarget,
            infoPlist: .file(path: "Sources/\(name)Tests/BookSeekerTests.plist"),
            sources: ["Sources/\(name)Tests/**"],
            dependencies: [.target(name: "\(name)")] + unitTestLibDependencies
        )

        let uiTestTarget = Target(
            name: "\(name)UITests",
            platform: platform,
            product: .uiTests,
            bundleId: "\(bundleId)\(name)UITests",
            deploymentTarget: deploymentTarget,
            infoPlist: .file(path: "Sources/\(name)UITests/BookSeekerUITests.plist"),
            sources: ["Sources/\(name)UITests/**"],
            dependencies: [.target(name: "\(name)")] + uiTestLibDependencies
        )

        return [mainTarget, testTarget, uiTestTarget]
    }

    /// Helper function to create the application schemes
    private static func makeAppSchemes(name: String) -> [Scheme] {
        let mainBuildAction: BuildAction = BuildAction(
            targets: [
                TargetReference(stringLiteral: "\(name)")
            ]
        )

        let mainTestAction: TestAction = TestAction.targets(
            [TestableTarget(target: "\(name)Tests", randomExecutionOrdering: true)],
            options: .options(coverage: true, codeCoverageTargets: [TargetReference(stringLiteral: "\(name)")]),
            diagnosticsOptions: [.mainThreadChecker]
        )

        let mainRunAction: RunAction = RunAction.runAction(
            configuration: .debug,
            preActions: [],
            postActions: [])

        let mainArchiveAction: ArchiveAction = ArchiveAction.archiveAction(configuration: .release)

        let mainProfileAction: ProfileAction = ProfileAction.profileAction(
            configuration: .release
        )
        let mainAnalyzeAction: AnalyzeAction = AnalyzeAction.analyzeAction(configuration: .debug)

        let mainScheme = Scheme(
            name: name,
            shared: true,
            buildAction: mainBuildAction,
            testAction: mainTestAction,
            runAction: mainRunAction,
            archiveAction: mainArchiveAction,
            profileAction: mainProfileAction,
            analyzeAction: mainAnalyzeAction
        )

        let uiTestAction: TestAction =  TestAction.targets(
            [TestableTarget(target: "\(name)UITests")],
            configuration: ConfigurationName.release,
            diagnosticsOptions: [.mainThreadChecker]
        )

        let uiTestScheme = Scheme(
            name: "UITest\(name)",
            shared: true,
            buildAction: mainBuildAction,
            testAction: uiTestAction
        )

        return [mainScheme, uiTestScheme]
    }

}
