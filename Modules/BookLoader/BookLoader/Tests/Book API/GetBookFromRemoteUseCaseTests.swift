//
//  GetBookFromSearchStringUseCaseTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi All rights reserved.
//

import XCTest
import BookLoader

class GetBookFromRemoteUseCaseTests: XCTestCase {

    func test_init_doesNotRequestDataFromURL() {
        let (_, client) = makeSUT()

        XCTAssertTrue(client.requestedURLs.isEmpty)
    }

    func test_load_requestsDataFromURL() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = makeSUT(url: url)

        sut.load(with: "term") { _ in }

        XCTAssertEqual(client.requestedURLs, [url])
    }

    func test_load_requestsDataFromURLWithCountryCode() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = makeSUT(url: url)
        let parameters: [String: String] = [
            "term": "term",
            "country": "BR",
            "entity": "ebook"]
        sut.load(with: "term", countryCode: "BR") { _ in }

        XCTAssertEqual(client.requestedURLs, [url])
        XCTAssertEqual(client.requestedParameters, [parameters])
    }

    func test_loadTwice_requestsDataFromURLTwice() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = makeSUT(url: url)
        sut.load(with: "term") { _ in }
        sut.load(with: "term") { _ in }
        XCTAssertEqual(client.requestedURLs, [url, url])
    }

    func test_load_requestsDataTwiceFromURLWithCountryCode() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = makeSUT(url: url)
        let parameters: [String: String] = [
            "term": "term",
            "country": "BR",
            "entity": "ebook"]
        sut.load(with: "term", countryCode: "BR") { _ in }
        sut.load(with: "term", countryCode: "BR") { _ in }
        XCTAssertEqual(client.requestedURLs, [url, url])
        XCTAssertEqual(client.requestedParameters, [parameters, parameters])
    }

    func test_load_deliversErrorOnClientError() {
        let (sut, client) = makeSUT()

        expect(
            sut,
            toCompleteWith: .failure(RemoteBookLoader.Error.connectivity),
            term: "term",
            when: {
            let clientError = NSError(domain: "Test", code: 0)
            client.complete(with: clientError)
        }
        )
    }

    func test_load_deliversErrorOnNon200HTTPResponse() {
        let (sut, client) = makeSUT()

        let samples = [199, 201, 300, 400, 500]

        samples.enumerated().forEach { index, code in
            expect(sut, toCompleteWith: .failure(RemoteBookLoader.Error.invalidData), term: "term", when: {
                let json = makeItemsJSON([])
                client.complete(withStatusCode: code, data: json, at: index)
            })
        }
    }

    func test_load_deliversErrorOn200HTTPResponseWithInvalidJSON() {
        let (sut, client) = makeSUT()

        expect(sut, toCompleteWith: .failure(RemoteBookLoader.Error.invalidData), term: "term", when: {
            let invalidJSON = Data("invalid json".utf8)
            client.complete(withStatusCode: 200, data: invalidJSON)
        })
    }

    func test_load_deliversNoItemsOn200HTTPResponseWithEmptyJSONList() {
        let (sut, client) = makeSUT()

        expect(sut, toCompleteWith: .success([]), term: "term", when: {
            let emptyListJSON = makeItemsJSON([])
            client.complete(withStatusCode: 200, data: emptyListJSON)
        })
    }

    func test_load_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
        let url = URL(string: "http://any-url.com")!
        let client = HTTPClientSpy()
        var sut: RemoteBookLoader? = RemoteBookLoader(url: url, client: client)

        var capturedResults = [RemoteBookLoader.Result]()
        sut?.load(with: "term", countryCode: nil) { capturedResults.append($0)
        }

        sut = nil
        client.complete(withStatusCode: 200, data: makeItemsJSON([]))

        XCTAssertTrue(capturedResults.isEmpty)
    }

    func test_load_shouldConvertDateToStringSuccessfully() {
        let dateComponents = DateComponents(
            calendar: Calendar(identifier: .gregorian),
            timeZone: TimeZone(abbreviation: "GMT"),
            year: 1980,
            month: 7,
            day: 11,
            hour: 8,
            minute: 34
        )

        let userCalendar = Calendar(identifier: .gregorian)
        let someDateTime = userCalendar.date(from: dateComponents)

        XCTAssertEqual(someDateTime?.toString, "1980-07-11T08:34:00Z")
    }

    func test_load_shouldConvertNilDateToStringSuccessfully() {
        let mDate: Date? = nil

        XCTAssertEqual(mDate.toString, "")
    }

    func test_load_shouldConvertStringToDateSuccessfully() {
        let dateString = "2014-04-15T07:00:00Z"
        let dateComponents = DateComponents(
            calendar: Calendar(identifier: .gregorian),
            timeZone: TimeZone(abbreviation: "GMT"),
            year: 2014,
            month: 4,
            day: 15,
            hour: 7,
            minute: 0
        )
        let userCalendar = Calendar(identifier: .gregorian)
        let someDateTime = userCalendar.date(from: dateComponents)

        XCTAssertEqual(dateString.toDate, someDateTime)
    }

    func test_load_shouldConvertEmptyStringToDateSuccessfully() {
        let dateString = ""
        let mDate: Date? = nil

        XCTAssertEqual(dateString.toDate, mDate)
    }
}
