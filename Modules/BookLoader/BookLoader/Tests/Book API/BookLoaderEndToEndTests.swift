//
//  BookLoaderEndToEndTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

class BookLoaderEndToEndTests: XCTestCase {

    func test_endToEndTestServerGETBookResult_matchesData() {
        switch getBookResult() {
        case let .success(books)?:
            XCTAssertEqual(books.count, 50, "Expected 50 images in the result")

        case let .failure(error)?:
            XCTFail("Expected successful result, got \(error) instead")

        default:
            XCTFail("Expected successful result, got no result instead")
        }
    }

    // MARK: - Helpers

    private func getBookResult(
        term: String = "swift",
        file: StaticString = #file,
        line: UInt = #line
    ) -> BookLoaderProtocol.Result? {
        let testServerURL = URL(string: "https://itunes.apple.com/search")!
        let client = URLSessionHTTPClient(session: URLSession(configuration: .ephemeral))
        let loader = RemoteBookLoader(url: testServerURL, client: client)
        trackForMemoryLeaks(client, file: file, line: line)
        trackForMemoryLeaks(loader, file: file, line: line)

        let exp = expectation(description: "Wait for load completion")

        var receivedResult: BookLoaderProtocol.Result?
        loader.load(with: term) { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)

        return receivedResult
    }
}
