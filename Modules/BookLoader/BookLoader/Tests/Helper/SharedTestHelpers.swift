//
//  SharedTestHelpers.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

func anyNSError() -> NSError {
    return NSError(domain: "any error", code: 0)
}

func anyURL() -> URL {
    return URL(string: "http://any-url.com")!
}

func anyURLWithParameters() -> URL {
    return URL(string: "http://any-url.com?country=BR&term=xpto")!
}

func anyURLWithAnotherOrderParameters() -> URL {
    return URL(string: "http://any-url.com?term=xpto&country=BR")!
}
