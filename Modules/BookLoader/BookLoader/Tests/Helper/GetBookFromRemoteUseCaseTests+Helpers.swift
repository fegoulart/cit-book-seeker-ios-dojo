//
//  GetBookFromRemoteUseCaseTests+Helpers.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 LeApi. All rights reserved.
//

import Foundation
import BookLoader
import XCTest

extension GetBookFromRemoteUseCaseTests {
    // MARK: - Helpers

    func makeSUT(
        url: URL = URL(string: "https://a-url.com")!,
        parameter: [String: String] = [:],
        file: StaticString = #file,
        line: UInt = #line
    ) -> (
        sut: RemoteBookLoader,
        client: HTTPClientSpy
    ) {
        let client = HTTPClientSpy()
        let sut = RemoteBookLoader(url: url, client: client)
        trackForMemoryLeaks(sut, file: file, line: line)
        trackForMemoryLeaks(client, file: file, line: line)
        return (sut, client)
    }

    class HTTPClientSpy: HTTPClient {
        private var messages = [(url: URL, parameters: [String: String], completion: (HTTPClient.Result) -> Void)]()

        var requestedURLs: [URL] {
            return messages.map { $0.url }
        }

        var requestedParameters: [[String: String]] {
            return messages.map { $0.parameters }
        }

        func get(
            from url: URL,
            with parameters: [String: String] = [:],
            completion: @escaping (HTTPClient.Result) -> Void
        ) {
            messages.append((url, parameters, completion))
        }

        func complete(with error: Error, at index: Int = 0) {
            messages[index].completion(.failure(error))
        }

        func complete(withStatusCode code: Int, data: Data, at index: Int = 0) {
            let response = HTTPURLResponse(
                url: requestedURLs[index],
                statusCode: code,
                httpVersion: nil,
                headerFields: nil
            )!
            messages[index].completion(.success((data, response)))
        }
    }

    func expect(_
                sut: RemoteBookLoader,
                toCompleteWith expectedResult: BookLoaderProtocol.Result,
                term: String,
                countryCode: String? = nil,
                when action: () -> Void,
                file: StaticString = #file,
                line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for load then")

        sut.load(with: term, countryCode: countryCode) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedItems), .success(expectedItems)):
                XCTAssertEqual(receivedItems, expectedItems, file: file, line: line)

            case let (
                .failure(receivedError as RemoteBookLoader.Error),
                .failure(expectedError as RemoteBookLoader.Error)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)

            default:
                XCTFail("Expected result \(expectedResult) got \(receivedResult) instead", file: file, line: line)
            }

            exp.fulfill()

        }
        action()

        wait(for: [exp], timeout: 1.0)
    }

    func makeBook(
        identifier: Int,
        title: String? = nil,
        description: String? = nil,
        editor: String? = nil,
        bookUrl: String? = nil,
        smalImageURL: String? = nil,
        largeImageUrl: String? = nil,
        userRating: Float? = nil,
        userRatingCount: Int? = nil,
        price: Float? = nil,
        currency: String? = nil,
        releaseDate: Date? = nil
    ) -> (model: Book, json: [String: Any]) {
        let price = Price(
            price: price,
            currency: currency
        )
        let bookImage = BookImage(
            smallImageUrl: smalImageURL,
            largeImageUrl: largeImageUrl
        )
        let rating = Rating(
            userRating: userRating,
            userRatingCount: userRatingCount
        )
        let item = Book(
            identifier: identifier,
            title: title,
            image: bookImage,
            description: description,
            editor: editor,
            price: price,
            rating: rating,
            bookUrl: bookUrl,
            releaseDate: releaseDate
        )

        let json = [
            "artworkUrl60": smalImageURL ?? "",
            "artworkUrl100": largeImageUrl ?? "",
            "trackViewUrl": bookUrl ?? "",
            "trackId": identifier,
            "trackName": title ?? "",
            "releaseDate": releaseDate.toString,
            "currency": currency ?? "",
            "description": description ?? "",
            "editor": editor ?? "",
            "price": price
        ].compactMapValues { $0 }

        return (item, json)
    }

    // swiftlint:disable force_try
    func makeItemsJSON(_ items: [[String: Any]]) -> Data {
        let json = ["resultCount": items.count, "results": items] as [String: Any]
        return try! JSONSerialization.data(withJSONObject: json)
    }
    // swiftlint:enable force_try

}
