//
//  ValidateBookCacheUseCaseTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

class ValidateBookCacheUseCaseTests: XCTestCase {

    func test_init_doesNotMessageStoreUponCreation() {
        let (_, store) = makeSUT()

        XCTAssertEqual(store.receivedMessages, [])
    }

    func test_validateCache_deletesCacheOnRetrievalError() {
        let (sut, store) = makeSUT()

        sut.validateCache(searchTerm: "swift", countryCode: nil)
        store.completeRetrieval(with: anyNSError())

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil), .deleteCachedBook("swift", nil)])
    }

    func test_validateCache_doesNotDeleteCacheOnEmptyCache() {
        let (sut, store) = makeSUT()

        sut.validateCache(searchTerm: "swift", countryCode: nil)
        store.completeRetrievalWithEmptyCache()

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_validateCache_doesNotDeleteNonExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: 1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.validateCache(searchTerm: "swift", countryCode: nil)
        store.completeRetrieval(with: book.local, timestamp: nonExpiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_validateCache_deletesCacheOnExpiration() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusBookCacheMaxAge()
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.validateCache(searchTerm: "swift", countryCode: nil)
        store.completeRetrieval(with: book.local, timestamp: expirationTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil), .deleteCachedBook("swift", nil)])
    }

    func test_validateCache_deletesExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: -1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.validateCache(searchTerm: "swift", countryCode: nil)
        store.completeRetrieval(with: book.local, timestamp: expiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil), .deleteCachedBook("swift", nil)])
    }

    func test_validateCache_doesNotDeleteInvalidCacheAfterSUTInstanceHasBeenDeallocated() {
        let store = BookStoreSpy()
        var sut: LocalBookLoader? = LocalBookLoader(store: store, currentDate: Date.init)

        sut?.validateCache(searchTerm: "swift", countryCode: nil)

        sut = nil
        store.completeRetrieval(with: anyNSError())

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    // MARK: - Helpers

    private func makeSUT(
        currentDate: @escaping () -> Date = Date.init,
        file: StaticString = #file, line: UInt = #line
    ) -> (sut: LocalBookLoader, store: BookStoreSpy) {
        let store = BookStoreSpy()
        let sut = LocalBookLoader(store: store, currentDate: currentDate)
        trackForMemoryLeaks(store, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, store)
    }
}
