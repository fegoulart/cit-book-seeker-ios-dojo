//
//  LoadBookFromCacheUseCaseTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

class LoadBookFromCacheUseCaseTests: XCTestCase {

    func test_init_doesNotMessageStoreUponCreation() {
        let (_, store) = makeSUT()

        XCTAssertEqual(store.receivedMessages, [])
    }

    func test_load_requestsCacheRetrieval() {
        let (sut, store) = makeSUT()

        sut.load(with: "swift", countryCode: nil) { _ in }

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_failsOnRetrievalError() {
        let (sut, store) = makeSUT()
        let retrievalError = anyNSError()

        expect(sut, toCompleteWith: .failure(retrievalError), when: {
            store.completeRetrieval(with: retrievalError)
        })
    }

    func test_load_deliversNoBooksOnEmptyCache() {
        let (sut, store) = makeSUT()

        expect(sut, toCompleteWith: .success([]), when: {
            store.completeRetrievalWithEmptyCache()
        })
    }

    func test_load_deliversCachedBooksOnNonExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: 1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, toCompleteWith: .success(book.models), when: {
            store.completeRetrieval(with: book.local, timestamp: nonExpiredTimestamp)
        })
    }

    func test_load_deliversNoImagesOnCacheExpiration() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusBookCacheMaxAge()
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, toCompleteWith: .success([]), when: {
            store.completeRetrieval(with: book.local, timestamp: expirationTimestamp)
        })
    }

    func test_load_deliversNoImagesOnExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: -1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, toCompleteWith: .success([]), when: {
            store.completeRetrieval(with: book.local, timestamp: expiredTimestamp)
        })
    }

    func test_load_hasNoSideEffectsOnRetrievalError() {
        let (sut, store) = makeSUT()

        sut.load(with: "swift", countryCode: nil) { _ in }
        store.completeRetrieval(with: anyNSError())

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_hasNoSideEffectsOnEmptyCache() {
        let (sut, store) = makeSUT()

        sut.load(with: "swift", countryCode: nil) { _ in }
        store.completeRetrievalWithEmptyCache()

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_hasNoSideEffectsOnNonExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: 1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.load(with: "swift", countryCode: nil) { _ in }
        store.completeRetrieval(with: book.local, timestamp: nonExpiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_hasNoSideEffectsOnCacheExpiration() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusBookCacheMaxAge()
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.load(with: "swift", countryCode: nil) { _ in }
        store.completeRetrieval(with: book.local, timestamp: expirationTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_hasNoSideEffectsOnExpiredCache() {
        let book = uniqueBook()
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: -1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.load(with: "swift", countryCode: nil) { _ in }
        store.completeRetrieval(with: book.local, timestamp: expiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieve("swift", nil)])
    }

    func test_load_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
        let store = BookStoreSpy()
        var sut: LocalBookLoader? = LocalBookLoader(store: store, currentDate: Date.init)

        var receivedResults = [LocalBookLoader.LoadResult]()
        sut?.load(with: "swift", countryCode: nil) { receivedResults.append($0) }

        sut = nil
        store.completeRetrievalWithEmptyCache()

        XCTAssertTrue(receivedResults.isEmpty)
    }

    // MARK: - Helpers

    private func makeSUT(
        currentDate: @escaping () -> Date = Date.init,
        file: StaticString = #file,
        line: UInt = #line
    ) -> (
        sut: LocalBookLoader,
        store: BookStoreSpy
    ) {
        let store = BookStoreSpy()
        let sut = LocalBookLoader(store: store, currentDate: currentDate)
        trackForMemoryLeaks(store, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, store)
    }

    private func expect(
        _ sut: LocalBookLoader,
        toCompleteWith expectedResult: LocalBookLoader.LoadResult,
        when action: () -> Void,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for load completion")

        sut.load(with: "swift", countryCode: nil) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedBooks), .success(expectedBooks)):
                XCTAssertEqual(receivedBooks, expectedBooks, file: file, line: line)

            case let (.failure(receivedError as NSError), .failure(expectedError as NSError)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)

            default:
                XCTFail("Expected result \(expectedResult), got \(receivedResult) instead", file: file, line: line)
            }

            exp.fulfill()
        }

        action()
        wait(for: [exp], timeout: 1.0)
    }
}
