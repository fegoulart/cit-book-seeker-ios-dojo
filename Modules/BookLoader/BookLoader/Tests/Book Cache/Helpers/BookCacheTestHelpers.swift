//
//  BookCacheTestHelpers.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation
import BookLoader

// swiftlint:disable line_length function_body_length
func uniqueBook() -> (models: [Book], local: [LocalBook]) {

    let title1 = "The Swift Programming Language (Swift 5.5)"
    let description1 = "Swift is a programming language for creating iOS, macOS, watchOS, and tvOS apps. Swift builds on the best of C and Objective-C, without the constraints of C compatibility. Swift adopts safe programming patterns and adds modern features to make programming easier, more flexible, and more fun. Swift’s clean slate, backed by the mature and much-loved Cocoa and Cocoa Touch frameworks, is an opportunity to reimagine how software development works.<br /><br />\nThis book provides:<br />\n- A tour of the language.<br />\n- A detailed guide delving into each language feature.<br />\n- A formal reference for the language."

    let book1 = Book(
        identifier: 881256329,
        title: title1,
        image: BookImage(
            smallImageUrl: "https://is1-ssl.mzstatic.com/image/thumb/Publication115/v4/57/5c/a6/575ca63f-d7e5-039a-8d4d-748cef2e689f/cover.jpg/60x60bb.jpg",
            largeImageUrl: "https://is1-ssl.mzstatic.com/image/thumb/Publication115/v4/57/5c/a6/575ca63f-d7e5-039a-8d4d-748cef2e689f/cover.jpg/100x100bb.jpg"
        ),
        description: description1,
        editor: "Apple Inc.",
        price: Price(price: 0.00, currency: "USD"),
        rating: Rating(
            userRating: 4.5,
            userRatingCount: 1707
        ),
        bookUrl: "https://books.apple.com/us/book/the-swift-programming-language-swift-5-5/id881256329?uo=4",
        releaseDate: "2014-06-02T07:00:00Z".toDate
    )

    let title2 = "Develop in Swift Fundamentals"
    let description2 = "Students build fundamental iOS app development skills with Swift. They’ll master the core concepts and practices that Swift programmers use daily and build a basic fluency in Xcode source and UI editors. Students will be able to create iOS apps that adhere to standard practices, including the use of stock UI elements, layout techniques, and common navigation interfaces. They’ll explore app design by brainstorming, planning, prototyping, and evaluating an app idea of their own. Three guided app projects help students build an app in Xcode from the ground up with step-by-step instructions. Xcode playgrounds help students learn key programming concepts as they write Swift code in an interactive coding environment that lets them experiment with code and see the results immediately."

    let book2 = Book(
        identifier: 1511184145,
        title: title2,
        image: BookImage(
            smallImageUrl: "https://is3-ssl.mzstatic.com/image/thumb/Publication114/v4/d9/2a/71/d92a712a-192d-cbe4-720a-102e61eaf440/1.2-DIS_Fundamentals.png/60x60bb.jpg",
            largeImageUrl: "https://is3-ssl.mzstatic.com/image/thumb/Publication114/v4/d9/2a/71/d92a712a-192d-cbe4-720a-102e61eaf440/1.2-DIS_Fundamentals.png/100x100bb.jpg"
        ),
        description: description2,
        editor: "Apple Education",
        price: Price(
            price: 0.00,
            currency: "USD"
        ),
        rating: Rating(
            userRating: 4.0,
            userRatingCount: 20
        ),
        bookUrl: "https://books.apple.com/us/book/develop-in-swift-fundamentals/id1511184145?uo=4",
        releaseDate: "2020-07-09T07:00:00Z".toDate
    )
    let models: [Book] = [book1, book2]
    let local = models.map { LocalBook(
        identifier: $0.identifier,
        title: $0.title,
        image: LocalBookImage(
            smallImageUrl: $0.image?.smallImageUrl,
            largeImageUrl: $0.image?.largeImageUrl
        ),
        description: $0.description,
        editor: $0.editor,
        price: LocalPrice(
            price: $0.price?.price,
            currency: $0.price?.currency
        ),
        rating: LocalRating(
            userRating: $0.rating?.userRating,
            userRatingCount: $0.rating?.userRatingCount
        ),
        bookUrl: $0.bookUrl,
        releaseDate: $0.releaseDate
    ) }
    return (models, local)
}
// swiftlint:enable line_length function_body_length

// swiftlint:disable line_length function_body_length
func latestUniqueBook() -> (models: [Book], local: [LocalBook]) {

    let title3 = "Swift"
    let description3 = "After a lengthy recovery period, Lufkal - a young zephyr pilot - is set to return to racing across the skies once more. But will he be able to handle the mounting pressure from his team and the rising tension from their troubled position?"

    let book3 = Book(
        identifier: 1412827225,
        title: title3,
        image: BookImage(
            smallImageUrl: "https://is1-ssl.mzstatic.com/image/thumb/Publication124/v4/9d/61/c0/9d61c01b-fd02-ecea-f9fa-05105ff3e3a5/9781387946143.jpg/60x60bb.jpg",
            largeImageUrl: "https://is1-ssl.mzstatic.com/image/thumb/Publication124/v4/9d/61/c0/9d61c01b-fd02-ecea-f9fa-05105ff3e3a5/9781387946143.jpg/100x100bb.jpg"
        ),
        description: description3,
        editor: "Myles Songolo",
        price: Price(price: 0.00, currency: "USD"),
        rating: nil,
        bookUrl: "https://books.apple.com/us/book/swift/id1412827225?uo=4",
        releaseDate: "2018-07-14T07:00:00Z".toDate
    )

    let title4 = "The Art of Racing In the Rain"
    let description4 = "Enzo knows he is different from other dogs: a philosopher with a nearly human soul (and an obsession with opposable thumbs), he has educated himself by watching television extensively, and by listening very closely to the words of his master, Denny Swift, an up-and-coming race car driver. <br /><br />Through Denny, Enzo has gained tremendous insight into the human condition, and he sees that life, like racing, isn't simply about going fast. Using the techniques needed on the race track, one can successfully navigate all of life's ordeals.<br /><br />On the eve of his death, Enzo takes stock of his life, recalling all that he and his family have been through: the sacrifices Denny has made to succeed professionally; the unexpected loss of Eve, Denny's wife; the three-year battle over their daughter, Zoë, whose maternal grandparents pulled every string to gain custody. In the end, despite what he sees as his own limitations, Enzo comes through heroically to preserve the Swift family, holding in his heart the dream that Denny will become a racing champion with Zoë at his side. Having learned what it takes to be a compassionate and successful person, the wise canine can barely wait until his next lifetime, when he is sure he will return as a man. <br /><br />A heart-wrenching but deeply funny and ultimately uplifting story of family, love, loyalty, and hope, The Art of Racing in the Rain is a beautifully crafted and captivating look at the wonders and absurdities of human life . . . as only a dog could tell it."

    let book4 = Book(
        identifier: 363686256,
        title: title4,
        image: BookImage(
            smallImageUrl: "https://is2-ssl.mzstatic.com/image/thumb/Publication114/v4/4e/ed/50/4eed5006-1b16-16be-e053-36ca4cd0211e/9780061738098.jpg/60x60bb.jpg",
            largeImageUrl: "https://is2-ssl.mzstatic.com/image/thumb/Publication114/v4/4e/ed/50/4eed5006-1b16-16be-e053-36ca4cd0211e/9780061738098.jpg/100x100bb.jpg"
        ),
        description: description4,
        editor: "Garth Stein",
        price: Price(
            price: 10.99,
            currency: "USD"
        ),
        rating: Rating(
            userRating: 4.5,
            userRatingCount: 6264
        ),
        bookUrl: "https://books.apple.com/us/book/the-art-of-racing-in-the-rain/id363686256?uo=4",
        releaseDate: "2009-03-17T07:00:00Z".toDate
    )
    let models: [Book] = [book3, book4]
    let local = models.map { LocalBook(
        identifier: $0.identifier,
        title: $0.title,
        image: LocalBookImage(
            smallImageUrl: $0.image?.smallImageUrl,
            largeImageUrl: $0.image?.largeImageUrl
        ),
        description: $0.description,
        editor: $0.editor,
        price: LocalPrice(
            price: $0.price?.price,
            currency: $0.price?.currency
        ),
        rating: LocalRating(
            userRating: $0.rating?.userRating,
            userRatingCount: $0.rating?.userRatingCount
        ),
        bookUrl: $0.bookUrl,
        releaseDate: $0.releaseDate
    ) }
    return (models, local)
}
// swiftlint:enable line_length function_body_length

extension Date {
    func minusBookCacheMaxAge() -> Date {
        return adding(days: -bookCacheMaxAgeInDays)
    }

    private var bookCacheMaxAgeInDays: Int {
        return 7
    }

    private func adding(days: Int) -> Date {
        return Calendar(identifier: .gregorian).date(byAdding: .day, value: days, to: self)!
    }
}

extension Date {
    func adding(seconds: TimeInterval) -> Date {
        return self + seconds
    }
}
