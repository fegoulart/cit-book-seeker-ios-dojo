//
//  BookStoreSpy.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation
import BookLoader

class BookStoreSpy: BookStoreProtocol {

    enum ReceivedMessage: Equatable {
        case deleteCachedBook(String, String?)
        case insert([LocalBook], String, String?, Date)
        case retrieve(String, String?)
        case retrieveSearchTerms(String?)
    }

    private(set) var receivedMessages = [ReceivedMessage]()

    private var deletionCompletions = [DeletionCompletion]()
    private var insertionCompletions = [InsertionCompletion]()
    private var retrievalCompletions = [RetrievalCompletion]()
    private var searchTermsRetrievalCompletions = [SearchTermRetrievalCompletion]()

    func deleteCachedBook(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping DeletionCompletion
    ) {
        deletionCompletions.append(completion)
        receivedMessages.append(.deleteCachedBook(searchTerm, countryCode))
    }

    func completeDeletion(with error: Error, at index: Int = 0) {
        deletionCompletions[index](.failure(error))
    }

    func completeDeletionSuccessfully(at index: Int = 0) {
        deletionCompletions[index](.success(()))
    }

    func insert(
        _ book: [LocalBook],
        searchTerm: String,
        countryCode: String?,
        timestamp: Date,
        completion: @escaping InsertionCompletion
    ) {
        insertionCompletions.append(completion)
        receivedMessages.append(.insert(book, searchTerm, countryCode, timestamp))
    }

    func completeInsertion(with error: Error, at index: Int = 0) {
        insertionCompletions[index](.failure(error))
    }

    func completeInsertionSuccessfully(at index: Int = 0) {
        insertionCompletions[index](.success(()))
    }

    func retrieve(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping RetrievalCompletion
    ) {
        retrievalCompletions.append(completion)
        receivedMessages.append(.retrieve(searchTerm, countryCode))
    }

    func completeRetrieval(with error: Error, at index: Int = 0) {
        retrievalCompletions[index](.failure(error))
    }

    func completeRetrievalWithEmptyCache(at index: Int = 0) {
        retrievalCompletions[index](.success(.none))
    }

    func completeRetrieval(with book: [LocalBook], timestamp: Date, at index: Int = 0) {
        retrievalCompletions[index](.success(CachedBook(book: book, timestamp: timestamp)))
    }

    func retrieveSearchTerms(
        countryCode: String?,
        completion: @escaping SearchTermRetrievalCompletion
    ) {
        searchTermsRetrievalCompletions.append(completion)
        receivedMessages.append(.retrieveSearchTerms(countryCode))
    }

    func completeSearchTermRetrieval(with error: Error, at index: Int = 0) {
        searchTermsRetrievalCompletions[index](.failure(error))
    }

    func completeSearchTermRetrievalWithEmptyCache(at index: Int = 0) {
        searchTermsRetrievalCompletions[index](.success(.none))
    }

    func completeSearchTermRetrieval(with searchTerms: [String], timestamp: Date, at index: Int = 0) {
        searchTermsRetrievalCompletions[index](.success(searchTerms.map { ($0, timestamp) }))
    }
}
