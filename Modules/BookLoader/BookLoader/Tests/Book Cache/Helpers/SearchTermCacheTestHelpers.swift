//
//  SearchTermCacheTestHelpers.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation
import BookLoader

// swiftlint:disable line_length function_body_length
func uniqueBRStreamingBook() -> (models: [Book], local: [LocalBook]) {

    let searchTermTitle1 = "To All the Boys I've Loved Before"
    let searchTermDescription1 = "<b><i>To All the Boys I’ve Loved Before </i>is now a major motion picture streaming on Netflix!</b><br /> <b>A <i>Time</i> Best YA Book of All Time (2021)</b><br /><br /><b>Lara Jean’s love life gets complicated in this<i> New York Times </i>bestselling “lovely, lighthearted romance” (<i>School Library Journal</i>) from the bestselling author of The Summer I Turned Pretty series.</b><br /><br />What if all the crushes you ever had found out how you felt about them…all at once?<br /><br />Sixteen-year-old Lara Jean Song keeps her love letters in a hatbox her mother gave her. They aren’t love letters that anyone else wrote for her; these are ones she’s written. One for every boy she’s ever loved—five in all. When she writes, she pours out her heart and soul and says all the things she would never say in real life, because her letters are for her eyes only. Until the day her secret letters are mailed, and suddenly, Lara Jean’s love life goes from imaginary to out of control."

    let searchTermBook1 = Book(
        identifier: 660012827,
        title: searchTermTitle1,
        image: BookImage(
            smallImageUrl: "https://is4-ssl.mzstatic.com/image/thumb/Publication123/v4/90/c4/2d/90c42d72-7b0d-2b25-f2e0-0e493521ba9a/9781442426726.jpg/60x60bb.jpg",
            largeImageUrl: "https://is4-ssl.mzstatic.com/image/thumb/Publication123/v4/90/c4/2d/90c42d72-7b0d-2b25-f2e0-0e493521ba9a/9781442426726.jpg/100x100bb.jpg"
        ),
        description: searchTermDescription1,
        editor: "Jenny Han",
        price: Price(price: 52.90, currency: "BRL"),
        rating: Rating(
            userRating: 4.5,
            userRatingCount: 12
        ),
        bookUrl: "https://books.apple.com/br/book/to-all-the-boys-ive-loved-before/id660012827?uo=4",
        releaseDate: "2014-04-15T07:00:00Z".toDate
    )

    let searchTermTitle2 = "Social Veg"
    let searchTermDescription2 = "Social Veg è nata per permettere a tutti coloro che lo desiderano di scoprire o approfondire il mondo vegetariano e vegano. Vuole essere uno spazio aperto al confronto e alla condivisione dove, in un ambiente accogliente come la cucina, sia possibile imparare nuove ricette, condividere le proprie esperienze, trovare risposte ad aspetti di natura alimentare, salutistica, medica, culturale &#xa0;e personale. Tutto questo con il massimo rispetto per ogni individuo in un’atmosfera serena, sociale, familiare e amichevole. Proprio come accade intorno ad un’allegra tavolata di amici."

    let searchTermBook2 = Book(
        identifier: 950254766,
        title: searchTermTitle2,
        image: BookImage(
            smallImageUrl: "https://is4-ssl.mzstatic.com/image/thumb/Publication1/v4/19/33/91/193391e6-783e-c73d-73be-8bdcafd21424/ibook_social_veg.jpg/60x60bb.jpg",
            largeImageUrl: "https://is4-ssl.mzstatic.com/image/thumb/Publication1/v4/19/33/91/193391e6-783e-c73d-73be-8bdcafd21424/ibook_social_veg.jpg/100x100bb.jpg"
        ),
        description: searchTermDescription2,
        editor: "Social Veg",
        price: Price(price: 0.00, currency: "BRL"),
        rating: nil,
        bookUrl: "https://books.apple.com/br/book/social-veg/id950254766?uo=4",
        releaseDate: "2014-10-12T07:00:00Z".toDate
    )

    let models: [Book] = [searchTermBook1, searchTermBook2]
    let local = models.map { LocalBook(
        identifier: $0.identifier,
        title: $0.title,
        image: LocalBookImage(
            smallImageUrl: $0.image?.smallImageUrl,
            largeImageUrl: $0.image?.largeImageUrl
        ),
        description: $0.description,
        editor: $0.editor,
        price: LocalPrice(
            price: $0.price?.price,
            currency: $0.price?.currency
        ),
        rating: LocalRating(
            userRating: $0.rating?.userRating,
            userRatingCount: $0.rating?.userRatingCount
        ),
        bookUrl: $0.bookUrl,
        releaseDate: $0.releaseDate
    ) }
    return (models, local)
}
// swiftlint:enable line_length function_body_length
