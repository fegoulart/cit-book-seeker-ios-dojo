//
//  FailableRetrieveSearchTermsBookStoreSpecs.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

extension FailableRetrieveSearchTermsBookStoreSpecs where Self: XCTestCase {
    func assertThatRetrieveSearchTermsDeliversFailureOnRetrievalError(
        on sut: BookStoreProtocol,
        countryCode: String?,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(sut, countryCode: countryCode, toRetrieveSearchTerms: .failure(anyNSError()), file: file, line: line)
    }

    func assertThatRetrieveSearchTermsHasNoSideEffectsOnFailure(
        on sut: BookStoreProtocol,
        countryCode: String?,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(
            sut,
            countryCode: countryCode,
            toRetrieveSearchTermsTwice: .failure(anyNSError()),
            file: file,
            line: line
        )
    }
}
