//
//  FailableInsertBookStoreSpecs.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

extension FailableInsertBookStoreSpecs where Self: XCTestCase {
    func assertThatInsertDeliversErrorOnInsertionError(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let insertionError = insert(
            book: uniqueBook().local,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: Date(),
            to: sut
        )

        XCTAssertNotNil(insertionError, "Expected cache insertion to fail with an error", file: file, line: line)
    }

    func assertThatInsertHasNoSideEffectsOnInsertionError(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        insert(
            book: uniqueBook().local,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: Date(),
            to: sut
        )

        expect(sut, toRetrieve: .success(.none), file: file, line: line)
    }
}
