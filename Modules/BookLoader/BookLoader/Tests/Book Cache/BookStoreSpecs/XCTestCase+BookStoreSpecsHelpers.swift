//
//  BookStoreSearchTermSpecs.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

extension BookStoreSpecs where Self: XCTestCase {
    @discardableResult
    func insert(
        book: [LocalBook],
        searchTerm: String,
        countryCode: String?,
        timestamp: Date,
        to sut: BookStoreProtocol
    ) -> Error? {
        let exp = expectation(description: "Wait for cache insertion")
        var insertionError: Error?
        sut.insert(
            book,
            searchTerm: searchTerm,
            countryCode: countryCode,
            timestamp: timestamp
        ) { result in
            if case let Result.failure(error) = result { insertionError = error }
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        return insertionError
    }

    @discardableResult
    func deleteCache(from sut: BookStoreProtocol) -> Error? {
        let exp = expectation(description: "Wait for cache deletion")
        var deletionError: Error?
        sut.deleteCachedBook(searchTerm: "swift", countryCode: nil) { result in
            if case let Result.failure(error) = result { deletionError = error }
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
        return deletionError
    }

    func expect(
        _ sut: BookStoreProtocol,
        toRetrieveTwice expectedResult: BookStoreProtocol.RetrievalResult,
        file: StaticString = #file, line: UInt = #line
    ) {
        expect(sut, toRetrieve: expectedResult, file: file, line: line)
        expect(sut, toRetrieve: expectedResult, file: file, line: line)
    }

    func expect(
        _ sut: BookStoreProtocol,
        toRetrieve expectedResult: BookStoreProtocol.RetrievalResult,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for cache retrieval")

        sut.retrieve(searchTerm: "swift", countryCode: nil) { retrievedResult in
            switch (expectedResult, retrievedResult) {
            case (.success(.none), .success(.none)),
                 (.failure, .failure):
                break

            case let (.success(.some(expected)), .success(.some(retrieved))):
                XCTAssertEqual(retrieved.book, expected.book, file: file, line: line)
                XCTAssertEqual(retrieved.timestamp, expected.timestamp, file: file, line: line)

            default:
                XCTFail(
                    "Expected to retrieve \(expectedResult), got \(retrievedResult) instead", file: file, line: line)
            }

            exp.fulfill()
        }

        wait(for: [exp], timeout: 1.0)
    }

    func expect(
        _ sut: BookStoreProtocol,
        countryCode: String?,
        toRetrieveSearchTermsTwice expectedResult: BookStoreProtocol.SearchTermRetrievalResult,
        file: StaticString = #file, line: UInt = #line
    ) {
        expect(sut, countryCode: countryCode, toRetrieveSearchTerms: expectedResult, file: file, line: line)
        expect(sut, countryCode: countryCode, toRetrieveSearchTerms: expectedResult, file: file, line: line)
    }

    func expect(
        _ sut: BookStoreProtocol,
        countryCode: String?,
        toRetrieveSearchTerms expectedResult: BookStoreProtocol.SearchTermRetrievalResult,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for cache retrieval")

        sut.retrieveSearchTerms(countryCode: countryCode) { retrievedResult in
            switch (expectedResult, retrievedResult) {
            case (.success(.none), .success(.none)),
                 (.failure, .failure):
                break

            case let (.success(.some(expected)), .success(.some(retrieved))):
                XCTAssertEqual(retrieved.map { $0.0 }, expected.map { $0.0 }, file: file, line: line)
                XCTAssertEqual(retrieved.map { $0.1 }, expected.map { $0.1 }, file: file, line: line)

            default:
                XCTFail(
                    "Expected to retrieve \(expectedResult), got \(retrievedResult) instead", file: file, line: line)
            }

            exp.fulfill()
        }

        wait(for: [exp], timeout: 1.0)
    }
}
