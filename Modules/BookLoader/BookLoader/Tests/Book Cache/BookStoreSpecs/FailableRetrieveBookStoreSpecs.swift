//
//  FailableRetrieveBookStoreSpecs.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

extension FailableRetrieveBookStoreSpecs where Self: XCTestCase {
    func assertThatRetrieveDeliversFailureOnRetrievalError(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(sut, toRetrieve: .failure(anyNSError()), file: file, line: line)
    }

    func assertThatRetrieveHasNoSideEffectsOnFailure(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(sut, toRetrieveTwice: .failure(anyNSError()), file: file, line: line)
    }
}
