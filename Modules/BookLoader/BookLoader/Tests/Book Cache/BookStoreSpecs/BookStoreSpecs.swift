//
//  BookStoreSpecs.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

protocol BookStoreSpecs {
    func test_retrieve_deliversEmptyOnEmptyCache()
    func test_retrieve_hasNoSideEffectsOnEmptyCache()
    func test_retrieve_deliversFoundValuesOnNonEmptyCache()
    func test_retrieve_hasNoSideEffectsOnNonEmptyCache()

    func test_retrieveSearchTerms_deliversEmptyOnEmptyCache()
    func test_retrieveSearchTerms_hasNoSideEffectsOnEmptyCache()
    func test_retrieveSearchTerms_deliversFoundValuesOnNonEmptyCache()
    func test_retrieveSearchTerms_deliversEmptyOnDifferentCountryCache()
    func test_retrieveSearchTerms_hasNoSideEffectsOnNonEmptyCache()

    func test_insert_deliversNoErrorOnEmptyCache()
    func test_insert_deliversNoErrorOnNonEmptyCache()
    func test_insert_overridesPreviouslyInsertedCacheValues()

    func test_delete_deliversNoErrorOnEmptyCache()
    func test_delete_hasNoSideEffectsOnEmptyCache()
    func test_delete_deliversNoErrorOnNonEmptyCache()
    func test_delete_emptiesPreviouslyInsertedCache()

    func test_storeSideEffects_runSerially()
}

protocol FailableRetrieveBookStoreSpecs: BookStoreSpecs {
    func test_retrieve_deliversFailureOnRetrievalError()
    func test_retrieve_hasNoSideEffectsOnFailure()
}

protocol FailableRetrieveSearchTermsBookStoreSpecs: BookStoreSpecs {
    func test_retrieveSearchTerm_deliversFailureOnRetrievalError()
    func test_retrieveSearchTerm_hasNoSideEffectsOnFailure()
}

protocol FailableInsertBookStoreSpecs: BookStoreSpecs {
    func test_insert_deliversErrorOnInsertionError()
    func test_insert_hasNoSideEffectsOnInsertionError()
}

protocol FailableDeleteBookStoreSpecs: BookStoreSpecs {
    func test_delete_deliversErrorOnDeletionError()
    func test_delete_hasNoSideEffectsOnDeletionError()
}

typealias FailableBookStoreSpecs =
FailableRetrieveBookStoreSpecs &
FailableRetrieveSearchTermsBookStoreSpecs &
FailableInsertBookStoreSpecs &
FailableDeleteBookStoreSpecs
