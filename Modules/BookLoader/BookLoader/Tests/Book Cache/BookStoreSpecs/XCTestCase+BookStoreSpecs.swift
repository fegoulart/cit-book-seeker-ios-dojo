//
//  XCTestCase+BookStoreSpecs.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

extension BookStoreSpecs where Self: XCTestCase {

    func assertThatRetrieveDeliversEmptyOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(sut, toRetrieve: .success(.none), file: file, line: line)
    }

    func assertThatRetrieveHasNoSideEffectsOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        expect(sut, toRetrieveTwice: .success(.none), file: file, line: line)
    }

    func assertThatRetrieveDeliversFoundValuesOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        expect(
            sut,
            toRetrieve: .success(CachedBook(book: book, timestamp: timestamp)),
            file: file,
            line: line
        )
    }

    func assertThatRetrieveHasNoSideEffectsOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        expect(sut, toRetrieveTwice: .success(CachedBook(book: book, timestamp: timestamp)), file: file, line: line)
    }

    func assertThatRetrieveSearchTermsDeliversEmptyOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let countryCode: String? = "BR"
        expect(sut, countryCode: countryCode, toRetrieveSearchTerms: .success(.none), file: file, line: line)
    }

    func assertThatRetrieveSearchTermsHasNoSideEffectsOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let countryCode: String? = "BR"
        expect(sut, countryCode: countryCode, toRetrieveSearchTermsTwice: .success(.none), file: file, line: line)
    }

    func assertThatRetrieveSearchTermsDeliversFoundValuesOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBRStreamingBook().local
        let searchTerm = "streaming"
        let countryCode = "BR"
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: searchTerm,
            countryCode: countryCode,
            timestamp: timestamp,
            to: sut
        )

        let anotherBook = uniqueBook().local
        let anotherSearchTerm = "swift"
        let anotherCountryCode = "BR"
        let anotherTimestamp = Date()

        insert(
            book: anotherBook,
            searchTerm: anotherSearchTerm,
            countryCode: anotherCountryCode,
            timestamp: anotherTimestamp,
            to: sut
        )

        expect(
            sut,
            countryCode: countryCode,
            toRetrieveSearchTerms: .success([(searchTerm, timestamp), (anotherSearchTerm, anotherTimestamp)]),
            file: file,
            line: line
        )
    }

    func assertThatRetrieveSearchTermsHasNoSideEffectsOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBRStreamingBook().local
        let searchTerm = "streaming"
        let countryCode = "BR"
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: searchTerm,
            countryCode: countryCode,
            timestamp: timestamp,
            to: sut
        )

        expect(
            sut,
            countryCode: countryCode,
            toRetrieveSearchTermsTwice: .success([(searchTerm, timestamp)]),
            file: file,
            line: line
        )
    }

    func assertThatRetrieveSearchTermsDeliversEmptyOnDifferentCountryCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBRStreamingBook().local
        let searchTerm = "streaming"
        let countryCode = "BR"
        let timestamp = Date()
        let differentCountryCode: String? = nil

        insert(
            book: book,
            searchTerm: searchTerm,
            countryCode: countryCode,
            timestamp: timestamp,
            to: sut
        )

        expect(sut, countryCode: differentCountryCode, toRetrieveSearchTerms: .success(.none), file: file, line: line)
    }

    func assertThatInsertDeliversNoErrorOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        let insertionError = insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        XCTAssertNil(insertionError, "Expected to insert cache successfully", file: file, line: line)
    }

    func assertThatInsertDeliversNoErrorOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        let insertionError = insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        XCTAssertNil(insertionError, "Expected to override cache successfully", file: file, line: line)
    }

    func assertThatInsertOverridesPreviouslyInsertedCacheValues(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        let latestBook = latestUniqueBook().local
        let latestTimestamp = Date()

        insert(
            book: latestBook,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: latestTimestamp,
            to: sut
        )

        expect(
            sut,
            toRetrieve: .success(
                CachedBook(
                    book: latestBook,
                    timestamp: latestTimestamp
                )
            ),
            file: file,
            line: line
        )
    }

    func assertThatDeleteDeliversNoErrorOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let deletionError = deleteCache(from: sut)

        XCTAssertNil(deletionError, "Expected empty cache deletion to succeed", file: file, line: line)
    }

    func assertThatDeleteHasNoSideEffectsOnEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        deleteCache(from: sut)

        expect(sut, toRetrieve: .success(.none), file: file, line: line)
    }

    func assertThatDeleteDeliversNoErrorOnNonEmptyCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        let deletionError = deleteCache(from: sut)

        XCTAssertNil(deletionError, "Expected non-empty cache deletion to succeed", file: file, line: line)
    }

    func assertThatDeleteEmptiesPreviouslyInsertedCache(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let book = uniqueBook().local
        let timestamp = Date()

        insert(
            book: book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp,
            to: sut
        )

        deleteCache(from: sut)

        expect(sut, toRetrieve: .success(.none), file: file, line: line)
    }

    func assertThatSideEffectsRunSerially(
        on sut: BookStoreProtocol,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        var completedOperationsInOrder = [XCTestExpectation]()

        let op1 = expectation(description: "Operation 1")
        let book = uniqueBook().local
        let timestamp = Date()
        sut.insert(
            book,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: timestamp) { _ in
            completedOperationsInOrder.append(op1)
            op1.fulfill()
        }

        let op2 = expectation(description: "Operation 2")
        sut.deleteCachedBook(
            searchTerm: "swift",
            countryCode: nil
        ) { _ in
            completedOperationsInOrder.append(op2)
            op2.fulfill()
        }

        let op3 = expectation(description: "Operation 3")
        sut.insert(
            uniqueBook().local,
            searchTerm: "swift",
            countryCode: nil,
            timestamp: Date()
        ) { _ in
            completedOperationsInOrder.append(op3)
            op3.fulfill()
        }

        waitForExpectations(timeout: 5.0)

        XCTAssertEqual(
            completedOperationsInOrder, [op1, op2, op3],
            "Expected side-effects to run serially but operations finished in the wrong order",
            file: file,
            line: line
        )
    }
}
