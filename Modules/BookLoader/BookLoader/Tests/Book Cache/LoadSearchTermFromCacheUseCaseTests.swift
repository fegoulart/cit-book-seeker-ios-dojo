//
//  LoadSearchTermFromCacheUseCaseTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

class LoadSearchTermFromCacheUseCaseTests: XCTestCase {

    func test_init_doesNotMessageStoreUponCreation() {
        let (_, store) = makeSUT()

        XCTAssertEqual(store.receivedMessages, [])
    }

    func test_load_requestsCacheRetrieval() {
        let (sut, store) = makeSUT()
        sut.loadPreviousSearchTerms(countryCode: nil) { _ in }

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms(nil)])
    }

    func test_load_failsOnRetrievalError() {
        let (sut, store) = makeSUT()
        let countryCode = "BR"
        let retrievalError = anyNSError()

        expect(sut, countryCode: countryCode, toCompleteWith: .failure(retrievalError), when: {
            store.completeSearchTermRetrieval(with: retrievalError)
        })
    }

    func test_load_deliversNoBooksOnEmptyCache() {
        let (sut, store) = makeSUT()
        let countryCode = "BR"

        expect(sut, countryCode: countryCode, toCompleteWith: .success([]), when: {
            store.completeSearchTermRetrievalWithEmptyCache()
        })
    }

    func test_load_deliversCachedSearchTermsOnNonExpiredCache() {
        let searchTerms: [String] = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: 1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, countryCode: countryCode, toCompleteWith: .success(searchTerms), when: {
            store.completeSearchTermRetrieval(with: searchTerms, timestamp: nonExpiredTimestamp)
        })
    }

    func test_load_deliversNoSearchTermsOnCacheExpiration() {
        let searchTerms: [String] = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusBookCacheMaxAge()
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, countryCode: countryCode, toCompleteWith: .success([]), when: {
            store.completeSearchTermRetrieval(with: searchTerms, timestamp: expirationTimestamp)
        })
    }

    func test_load_deliversNoSearchTermOnExpiredCache() {
        let searchTerm = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: -1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        expect(sut, countryCode: countryCode, toCompleteWith: .success([]), when: {
            store.completeSearchTermRetrieval(with: searchTerm, timestamp: expiredTimestamp)
        })
    }

    func test_load_hasNoSideEffectsOnRetrievalError() {
        let (sut, store) = makeSUT()
        let countryCode = "BR"

        sut.loadPreviousSearchTerms(countryCode: countryCode) { _ in }
        store.completeSearchTermRetrieval(with: anyNSError())

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms("BR")])
    }

    func test_load_hasNoSideEffectsOnEmptyCache() {
        let (sut, store) = makeSUT()
        let countryCode = "BR"

        sut.loadPreviousSearchTerms(countryCode: countryCode) { _ in }
        store.completeSearchTermRetrievalWithEmptyCache()

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms("BR")])
    }

    func test_load_hasNoSideEffectsOnNonExpiredCache() {
        let searchTerm = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let nonExpiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: 1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.loadPreviousSearchTerms(countryCode: countryCode) { _ in }
        store.completeSearchTermRetrieval(with: searchTerm, timestamp: nonExpiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms(countryCode)])
    }

    func test_load_hasNoSideEffectsOnCacheExpiration() {
        let searchTerm = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let expirationTimestamp = fixedCurrentDate.minusBookCacheMaxAge()
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.loadPreviousSearchTerms(countryCode: countryCode) { _ in }
        store.completeSearchTermRetrieval(with: searchTerm, timestamp: expirationTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms(countryCode)])
    }

    func test_load_hasNoSideEffectsOnExpiredCache() {
        let searchTerm = ["swift"]
        let countryCode = "BR"
        let fixedCurrentDate = Date()
        let expiredTimestamp = fixedCurrentDate.minusBookCacheMaxAge().adding(seconds: -1)
        let (sut, store) = makeSUT(currentDate: { fixedCurrentDate })

        sut.loadPreviousSearchTerms(countryCode: countryCode) { _ in }
        store.completeSearchTermRetrieval(with: searchTerm, timestamp: expiredTimestamp)

        XCTAssertEqual(store.receivedMessages, [.retrieveSearchTerms(countryCode)])
    }

    func test_load_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
        let countryCode = "BR"
        let store = BookStoreSpy()
        var sut: LocalBookLoader? = LocalBookLoader(store: store, currentDate: Date.init)

        var receivedResults = [LocalBookLoader.LoadSearchTermResult]()
        sut?.loadPreviousSearchTerms(countryCode: countryCode) { receivedResults.append($0) }

        sut = nil
        store.completeSearchTermRetrievalWithEmptyCache()

        XCTAssertTrue(receivedResults.isEmpty)
    }

    // MARK: - Helpers

    private func makeSUT(
        currentDate: @escaping () -> Date = Date.init,
        file: StaticString = #file,
        line: UInt = #line
    ) -> (
        sut: LocalBookLoader,
        store: BookStoreSpy
    ) {
        let store = BookStoreSpy()
        let sut = LocalBookLoader(store: store, currentDate: currentDate)
        trackForMemoryLeaks(store, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, store)
    }

    private func expect(
        _ sut: LocalBookLoader,
        countryCode: String?,
        toCompleteWith expectedResult: LocalBookLoader.LoadSearchTermResult,
        when action: () -> Void,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for load completion")

        sut.loadPreviousSearchTerms(countryCode: countryCode) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(received), .success(expected)):
                XCTAssertEqual(received, expected, file: file, line: line)

            case let (.failure(receivedError as NSError), .failure(expectedError as NSError)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)

            default:
                XCTFail("Expected result \(expectedResult), got \(receivedResult) instead", file: file, line: line)
            }

            exp.fulfill()
        }

        action()
        wait(for: [exp], timeout: 1.0)
    }
}
