//
//  CacheBookUseCaseTests.swift
//  BookLoaderTests
//
//  Created by Fernando Luiz Goulart on 13/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import XCTest
import BookLoader

class CacheBookUseCaseTests: XCTestCase {
    func test_init_doesNotMessageStoreUponCreation() {
        let (_, store) = makeSUT()

        XCTAssertEqual(store.receivedMessages, [])
    }

    func test_save_requestsCacheDeletion() {
        let (sut, store) = makeSUT()

        sut.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { _ in }

        XCTAssertEqual(store.receivedMessages, [.deleteCachedBook("swift", nil)])
    }

    func test_save_doesNotRequestCacheInsertionOnDeletionError() {
        let (sut, store) = makeSUT()
        let deletionError = anyNSError()

        sut.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { _ in }
        store.completeDeletion(with: deletionError)

        XCTAssertEqual(store.receivedMessages, [.deleteCachedBook("swift", nil)])
    }

    func test_save_requestsNewCacheInsertionWithTimestampOnSuccessfulDeletion() {
        let timestamp = Date()
        let book = uniqueBook()
        let (sut, store) = makeSUT(currentDate: { timestamp })

        sut.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { _ in }
        store.completeDeletionSuccessfully()

        XCTAssertEqual(
            store.receivedMessages,
            [.deleteCachedBook(
                "swift", nil
            ),
                .insert(
                    book.local,
                    "swift",
                    nil,
                    timestamp
                )
            ]
        )
    }

    func test_save_failsOnDeletionError() {
        let (sut, store) = makeSUT()
        let deletionError = anyNSError()

        expect(sut, toCompleteWithError: deletionError, when: {
            store.completeDeletion(with: deletionError)
        })
    }

    func test_save_failsOnInsertionError() {
        let (sut, store) = makeSUT()
        let insertionError = anyNSError()

        expect(sut, toCompleteWithError: insertionError, when: {
            store.completeDeletionSuccessfully()
            store.completeInsertion(with: insertionError)
        })
    }

    func test_save_succeedsOnSuccessfulCacheInsertion() {
        let (sut, store) = makeSUT()

        expect(sut, toCompleteWithError: nil, when: {
            store.completeDeletionSuccessfully()
            store.completeInsertionSuccessfully()
        })
    }

    func test_save_doesNotDeliverDeletionErrorAfterSUTInstanceHasBeenDeallocated() {
        let store = BookStoreSpy()
        var sut: LocalBookLoader? = LocalBookLoader(store: store, currentDate: Date.init)

        var receivedResults = [LocalBookLoader.SaveResult]()
        sut?.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { receivedResults.append($0) }

        sut = nil
        store.completeDeletion(with: anyNSError())

        XCTAssertTrue(receivedResults.isEmpty)
    }

    func test_save_doesNotDeliverInsertionErrorAfterSUTInstanceHasBeenDeallocated() {
        let store = BookStoreSpy()
        var sut: LocalBookLoader? = LocalBookLoader(store: store, currentDate: Date.init)

        var receivedResults = [LocalBookLoader.SaveResult]()
        sut?.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { receivedResults.append($0) }

        store.completeDeletionSuccessfully()
        sut = nil
        store.completeInsertion(with: anyNSError())

        XCTAssertTrue(receivedResults.isEmpty)
    }

    // MARK: - Helpers

    private func makeSUT(
        currentDate: @escaping () -> Date = Date.init,
        file: StaticString = #file,
        line: UInt = #line
    ) -> (
        sut: LocalBookLoader,
        store: BookStoreSpy
    ) {
        let store = BookStoreSpy()
        let sut = LocalBookLoader(store: store, currentDate: currentDate)
        trackForMemoryLeaks(store, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, store)
    }

    private func expect(
        _ sut: LocalBookLoader,
        toCompleteWithError expectedError: NSError?,
        when action: () -> Void,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        let exp = expectation(description: "Wait for save completion")

        var receivedError: Error?
        sut.save(uniqueBook().models, searchTerm: "swift", countryCode: nil) { result in
            if case let Result.failure(error) = result { receivedError = error }
            exp.fulfill()
        }

        action()
        wait(for: [exp], timeout: 1.0)

        XCTAssertEqual(receivedError as NSError?, expectedError, file: file, line: line)
    }
}
