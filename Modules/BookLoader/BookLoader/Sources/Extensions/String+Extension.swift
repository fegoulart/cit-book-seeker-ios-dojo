//
//  String+Extension.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

extension String {
    public var toDate: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: self)
        return date
    }
}
