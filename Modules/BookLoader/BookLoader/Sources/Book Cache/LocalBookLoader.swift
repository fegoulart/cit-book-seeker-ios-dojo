//
//  LocalBookLoader.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public final class LocalBookLoader {
    private let store: BookStoreProtocol
    private let currentDate: () -> Date

    public init(store: BookStoreProtocol, currentDate: @escaping () -> Date) {
        self.store = store
        self.currentDate = currentDate
    }
}

extension LocalBookLoader: BookCacheProtocol {
    public typealias SaveResult = BookCacheProtocol.Result

    public func save(
        _ book: [Book],
        searchTerm: String,
        countryCode: String?,
        completion: @escaping (SaveResult) -> Void
    ) {
        store.deleteCachedBook(
            searchTerm: searchTerm,
            countryCode: countryCode
        ) { [weak self] deletionResult in
            guard let self = self else { return }

            switch deletionResult {
            case .success:
                self.cache(
                    book,
                    searchTerm: searchTerm,
                    countryCode: countryCode,
                    with: completion
                )

            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    private func cache(_ book: [Book],
                       searchTerm: String,
                       countryCode: String?,
                       with completion: @escaping (SaveResult) -> Void
    ) {
        store.insert(
            book.toLocal(),
            searchTerm: searchTerm,
            countryCode: countryCode,
            timestamp: currentDate()
        ) { [weak self] insertionResult in
            guard self != nil else { return }

            completion(insertionResult)
        }
    }
}

extension LocalBookLoader: BookLoaderProtocol {
    public typealias LoadResult = BookLoaderProtocol.Result

    public func load(
        with term: String,
        countryCode: String? = nil,
        completion: @escaping (LoadResult) -> Void
    ) {
        store.retrieve(
            searchTerm: term,
            countryCode: countryCode
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case let .failure(error):
                completion(.failure(error))

            case let .success(.some(cache)) where
                BookCachePolicy.validate(cache.timestamp, against: self.currentDate()):
                completion(.success(cache.book.toModels()))

            case .success:
                completion(.success([]))
            }
        }
    }
}

extension LocalBookLoader {
    public func validateCache(
        searchTerm: String,
        countryCode: String?
    ) {
        store.retrieve(
            searchTerm: searchTerm,
            countryCode: countryCode
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure:
                self.store.deleteCachedBook(
                    searchTerm: searchTerm,
                    countryCode: countryCode
                ) { _ in }

            case let .success(.some(cache))
                where !BookCachePolicy.validate(cache.timestamp, against: self.currentDate()):
                self.store.deleteCachedBook(
                    searchTerm: searchTerm,
                    countryCode: countryCode
                ) { _ in }

            case .success: break
            }
        }
    }
}

extension LocalBookLoader: PreviousSearchTermLoaderProtocol {

    public typealias LoadSearchTermResult = PreviousSearchTermLoaderProtocol.Result

    public func loadPreviousSearchTerms(
        countryCode: String?,
        completion: @escaping (LoadSearchTermResult
        ) -> Void) {
        store.retrieveSearchTerms(countryCode: countryCode) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .failure(error):
                completion(.failure(error))

            case let .success(.some(searchTerms)):
                completion(.success(searchTerms.compactMap {
                    if BookCachePolicy.validate($0.1, against: self.currentDate()) {
                        return $0.0
                    }
                    return nil
                }))

            case .success:
                completion(.success([]))
            }
        }
    }
}

private extension Array where Element == Book {
    func toLocal() -> [LocalBook] {
        return map {
            let image = LocalBookImage(
                smallImageUrl: $0.image?.smallImageUrl,
                largeImageUrl: $0.image?.largeImageUrl
            )
            let price = LocalPrice(
                price: $0.price?.price,
                currency: $0.price?.currency
            )
            let rating = LocalRating(
                userRating: $0.rating?.userRating,
                userRatingCount: $0.rating?.userRatingCount
            )
            return LocalBook(
                identifier: $0.identifier,
                title: $0.title,
                image: image,
                description: $0.description,
                editor: $0.editor,
                price: price,
                rating: rating,
                bookUrl: $0.bookUrl,
                releaseDate: $0.releaseDate
            )
        }
    }
}

private extension Array where Element == LocalBook {
    func toModels() -> [Book] {
        return map {
            let image = BookImage(
                smallImageUrl: $0.image?.smallImageUrl,
                largeImageUrl: $0.image?.largeImageUrl
            )
            let price = Price(
                price: $0.price?.price,
                currency: $0.price?.currency
            )
            let rating = Rating(
                userRating: $0.rating?.userRating,
                userRatingCount: $0.rating?.userRatingCount
            )
            return Book(
                identifier: $0.identifier,
                title: $0.title,
                image: image,
                description: $0.description,
                editor: $0.editor,
                price: price,
                rating: rating,
                bookUrl: $0.bookUrl,
                releaseDate: $0.releaseDate
            )
        }
    }
}
