//
//  LocalBook.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public struct LocalBook: Equatable {
    public var identifier: Int
    public var title: String?
    public var image: LocalBookImage?
    public var description: String?
    public var editor: String?
    public var price: LocalPrice?
    public var rating: LocalRating?
    public var bookUrl: String?
    public var releaseDate: Date?

    public init(
        identifier: Int,
        title: String?,
        image: LocalBookImage?,
        description: String?,
        editor: String?,
        price: LocalPrice?,
        rating: LocalRating?,
        bookUrl: String?,
        releaseDate: Date?
    ) {
            self.identifier = identifier
            self.title = title
            self.image = image
            self.editor = editor
            self.price = price
            self.rating = rating
            self.bookUrl = bookUrl
            self.releaseDate = releaseDate
        }
}

public struct LocalRating: Equatable {
    public var userRating: Float?
    public var userRatingCount: Int?

    public init(
        userRating: Float?,
        userRatingCount: Int?
    ) {
        self.userRating = userRating
        self.userRatingCount = userRatingCount
    }
}

public struct LocalPrice: Equatable {
    public var price: Float?
    public var currency: String?

    public init(
        price: Float?,
        currency: String?
    ) {
        self.price = price
        self.currency = currency
    }
}

public struct LocalBookImage: Equatable {
    public var smallImageUrl: String?
    public var largeImageUrl: String?

    public init(
        smallImageUrl: String?,
        largeImageUrl: String?
    ) {
        self.smallImageUrl = smallImageUrl
        self.largeImageUrl = largeImageUrl
    }
}
