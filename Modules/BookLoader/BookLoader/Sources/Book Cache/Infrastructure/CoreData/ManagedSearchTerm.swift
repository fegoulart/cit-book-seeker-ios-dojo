//
//  ManagedSearchTerm.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import CoreData

@objc(ManagedSearchTerm)
class ManagedSearchTerm: NSManagedObject {
    @NSManaged var searchTerm: String
    @NSManaged var countryCode: String?
    @NSManaged var book: NSOrderedSet
    @NSManaged var timestamp: Date
}

extension ManagedSearchTerm {
    static func find(
        searchTerm: String,
        countryCode: String?,
        in context: NSManagedObjectContext
    ) throws -> ManagedSearchTerm? {
        let request = NSFetchRequest<ManagedSearchTerm>(entityName: entity().name!)
        let searchTermPredicate = NSPredicate(format: "searchTerm == %@", searchTerm)
        let countryCodePredicate = NSPredicate(format: "countryCode == %@", countryCode ?? NSNull())
        request.predicate = NSCompoundPredicate.init(
            type: .and,
            subpredicates: [searchTermPredicate, countryCodePredicate]
        )
        request.returnsObjectsAsFaults = false
        return try context.fetch(request).first
    }

    static func findSearchTerms(
        countryCode: String?,
        in context: NSManagedObjectContext
    ) throws -> ([(String, Date)]?) {
        let request = NSFetchRequest<ManagedSearchTerm>(entityName: entity().name!)
        request.predicate = NSPredicate(format: "countryCode == %@", countryCode ?? NSNull())
        request.returnsObjectsAsFaults = false
        let result = try context.fetch(request)
        guard !result.isEmpty else {
            return nil
        }
        return result.map { ($0.searchTerm, $0.timestamp) }
    }

    static func newUniqueInstance(
        searchTerm: String,
        countryCode: String?,
        in context: NSManagedObjectContext
    ) throws -> ManagedSearchTerm {
        try find(
            searchTerm: searchTerm,
            countryCode: countryCode,
            in: context
        ).map(context.delete)
        return ManagedSearchTerm(context: context)
    }

    var localBook: [LocalBook] {
        return book.compactMap { ($0 as? ManagedBook)?.local }
    }
}
