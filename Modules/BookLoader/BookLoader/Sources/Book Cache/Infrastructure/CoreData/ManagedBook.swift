//
//  ManagedBook.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import CoreData

@objc(ManagedBook)
class ManagedBook: NSManagedObject {
    @NSManaged var identifier: Int
    @NSManaged var title: String?
    @NSManaged var smallImageUrl: String?
    @NSManaged var largeImageUrl: String?
    @NSManaged var bookDescription: String?
    @NSManaged var editor: String?
    @NSManaged var price: NSNumber?
    @NSManaged var currency: String?
    @NSManaged var userRating: NSNumber?
    @NSManaged var userRatingCount: NSNumber?
    @NSManaged var bookUrl: String?
    @NSManaged var releaseDate: Date?
    @NSManaged var searchTerm: ManagedSearchTerm
}

extension ManagedBook {
    static func books(from localBook: [LocalBook], in context: NSManagedObjectContext) -> NSOrderedSet {
        return NSOrderedSet(array: localBook.map { local in
            let managed = ManagedBook(context: context)
            managed.identifier = local.identifier
            managed.title = local.title
            managed.smallImageUrl = local.image?.smallImageUrl
            managed.largeImageUrl = local.image?.largeImageUrl
            managed.bookDescription = local.description
            managed.editor = local.editor
            if let price = local.price?.price {
                managed.price = NSNumber(value: price)
            } else {
                managed.price = nil
            }
            managed.currency = local.price?.currency
            if let rating = local.rating?.userRating {
                managed.userRating = NSNumber(value: rating)
            } else {
                managed.userRating = nil
            }
            if let ratingCount = local.rating?.userRatingCount {
            managed.userRatingCount = NSNumber(value: ratingCount)
            } else {
                managed.userRatingCount = nil
            }
            managed.bookUrl = local.bookUrl
            managed.releaseDate = local.releaseDate
            return managed
        })
    }

    var local: LocalBook {
        let image = LocalBookImage(
            smallImageUrl: smallImageUrl,
            largeImageUrl: largeImageUrl
        )
        let rating = LocalRating(
            userRating: userRating as? Float,
            userRatingCount: userRatingCount as? Int
        )
        let price = LocalPrice(
            price: price as? Float,
            currency: currency
        )
        return LocalBook(
            identifier: identifier,
            title: title,
            image: image,
            description: bookDescription,
            editor: editor,
            price: price,
            rating: rating,
            bookUrl: bookUrl,
            releaseDate: releaseDate
        )
    }
}
