//
//  CoreDataBookStore.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import CoreData

public final class CoreDataBookStore: BookStoreProtocol {

    private let container: NSPersistentContainer
    private let context: NSManagedObjectContext

    enum StoreError: Error {
        case modelNotFound
        case failedToLoadPersistentContainer(Error)
    }

    public init(storeURL: URL, bundle: Bundle = .main) throws {
        container = try NSPersistentContainer.load(
            modelName: "BookStore",
            url: storeURL,
            in: bundle
        )
        context = container.newBackgroundContext()
    }

    public func retrieve(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping RetrievalCompletion
    ) {
        perform { context in
            completion(Result {
                try ManagedSearchTerm.find(
                    searchTerm: searchTerm,
                    countryCode: countryCode,
                    in: context
                ).map {
                    return CachedBook(book: $0.localBook, timestamp: $0.timestamp)
                }
            })
        }
    }

    public func retrieveSearchTerms(
        countryCode: String?,
        completion: @escaping SearchTermRetrievalCompletion
    ) {
        perform { context in
            completion(Result {
                try ManagedSearchTerm.findSearchTerms(
                    countryCode: countryCode,
                    in: context
                )
            })
        }
    }

    public func insert(
        _ book: [LocalBook],
        searchTerm: String,
        countryCode: String?,
        timestamp: Date,
        completion: @escaping InsertionCompletion
    ) {
        perform { context in
            completion(Result {
                let managedCache = try ManagedSearchTerm.newUniqueInstance(
                    searchTerm: searchTerm,
                    countryCode: countryCode,
                    in: context
                )
                managedCache.searchTerm = searchTerm
                managedCache.countryCode = countryCode
                managedCache.timestamp = timestamp
                managedCache.book = ManagedBook.books(from: book, in: context)
                try context.save()
            })
        }
    }

    public func deleteCachedBook(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping DeletionCompletion
    ) {
        perform { context in
            completion(Result {
                try ManagedSearchTerm.find(
                    searchTerm: searchTerm,
                    countryCode: countryCode,
                    in: context).map(context.delete).map(context.save)
            })
        }
    }

    private func perform(_ action: @escaping (NSManagedObjectContext) -> Void) {
        let context = self.context
        context.perform { action(context) }
    }

    private func cleanUpReferencesToPersistentStores() {
        context.performAndWait {
            let coordinator = self.container.persistentStoreCoordinator
            try? coordinator.persistentStores.forEach(coordinator.remove)
        }
    }

    deinit {
        cleanUpReferencesToPersistentStores()
    }
}
