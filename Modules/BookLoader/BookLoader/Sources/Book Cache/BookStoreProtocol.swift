//
//  BookStoreProtocol.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public typealias CachedBook = (book: [LocalBook], timestamp: Date)

public protocol BookStoreProtocol {
    typealias DeletionResult = Result<Void, Error>
    typealias DeletionCompletion = (DeletionResult) -> Void

    typealias InsertionResult = Result<Void, Error>
    typealias InsertionCompletion = (InsertionResult) -> Void

    typealias RetrievalResult = Result<CachedBook?, Error>
    typealias RetrievalCompletion = (RetrievalResult) -> Void

    typealias SearchTermRetrievalResult = Result<[(String, Date)]?, Error>
    typealias SearchTermRetrievalCompletion = (SearchTermRetrievalResult) -> Void

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func deleteCachedBook(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping DeletionCompletion
    )

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func insert(
        _ book: [LocalBook],
        searchTerm: String,
        countryCode: String?,
        timestamp: Date,
        completion: @escaping InsertionCompletion
    )

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func retrieve(
        searchTerm: String,
        countryCode: String?,
        completion: @escaping RetrievalCompletion
    )

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func retrieveSearchTerms(
        countryCode: String?,
        completion: @escaping SearchTermRetrievalCompletion
    )
}
