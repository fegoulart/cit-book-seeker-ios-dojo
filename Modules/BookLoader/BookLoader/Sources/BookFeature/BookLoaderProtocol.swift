//
//  BookLoaderProtocol.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

public protocol BookLoaderProtocol {
    typealias Result = Swift.Result<[Book], Error>

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func load(with term: String, countryCode: String?, completion: @escaping (Result) -> Void)
}
