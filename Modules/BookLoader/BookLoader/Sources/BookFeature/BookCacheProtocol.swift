//
//  BookCacheProtocol.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation

public protocol BookCacheProtocol {
    typealias Result = Swift.Result<Void, Error>

    func save(
        _ book: [Book],
        searchTerm: String,
        countryCode: String?,
        completion: @escaping (Result) -> Void
    )
}
