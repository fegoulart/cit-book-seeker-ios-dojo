//
//  Book.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public struct Book: Equatable {
    public var identifier: Int
    public var title: String?
    public var image: BookImage?
    public var description: String?
    public var editor: String?
    public var price: Price?
    public var rating: Rating?
    public var bookUrl: String?
    public var releaseDate: Date?

    public init(
        identifier: Int,
        title: String?,
        image: BookImage?,
        description: String?,
        editor: String?,
        price: Price?,
        rating: Rating?,
        bookUrl: String?,
        releaseDate: Date?
    ) {
            self.identifier = identifier
            self.title = title
            self.image = image
            self.editor = editor
            self.price = price
            self.rating = rating
            self.bookUrl = bookUrl
            self.releaseDate = releaseDate
        }
}

public struct Rating: Equatable {
    public var userRating: Float?
    public var userRatingCount: Int?

    public init(
        userRating: Float?,
        userRatingCount: Int?
    ) {
        self.userRating = userRating
        self.userRatingCount = userRatingCount
    }
}

public struct Price: Equatable {
    public var price: Float?
    public var currency: String?

    public init(
        price: Float?,
        currency: String?
    ) {
        self.price = price
        self.currency = currency
    }
}

public struct BookImage: Equatable {
    public var smallImageUrl: String?
    public var largeImageUrl: String?

    public init(
        smallImageUrl: String?,
        largeImageUrl: String?
    ) {
        self.smallImageUrl = smallImageUrl
        self.largeImageUrl = largeImageUrl
    }
}
