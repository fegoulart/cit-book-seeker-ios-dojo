//
//  SearchTermLoaderProtocol.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 12/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public protocol PreviousSearchTermLoaderProtocol {
    typealias Result = Swift.Result<[String], Error>

    /// The completion handler can be invoked in any thread.
    /// Clients are responsible to dispatch to appropriate threads, if needed.
    func loadPreviousSearchTerms(countryCode: String?, completion: @escaping (Result) -> Void)
}
