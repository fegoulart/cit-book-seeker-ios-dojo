//
//  RemoteBook.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

public struct RemoteBook: Decodable {
    let artworkUrl60: String?
    let artworkUrl100: String?
    let trackViewUrl: String?
    let trackId: Int?
    let trackName: String?
    let releaseDate: String?
    let currency: String?
    let description: String?
    let artistName: String?
    let price: Float?
    let averageUserRating: Float?
    let userRatingCount: Int?
}
