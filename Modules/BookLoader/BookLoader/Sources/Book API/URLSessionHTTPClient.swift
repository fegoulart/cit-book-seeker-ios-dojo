//
//  URLSessionHTTPClient.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public class URLSessionHTTPClient: HTTPClient {

    private let session: URLSession

    public init(session: URLSession = .shared) {
        self.session = session
    }

    private struct UnexpectedValuesRepresentation: Error {}
    public struct CouldNotInitializeURLRequest: Error {}

    public func get(from url: URL,
                    with parameters: [String: String] = [:],
                    completion: @escaping (HTTPClient.Result) -> Void) throws {

        guard
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            throw CouldNotInitializeURLRequest()
        }
        if parameters.isNotEmpty {
            components.queryItems = parameters.map { (key, value) in
                URLQueryItem(name: key, value: value)
            }
            let percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            components.percentEncodedQuery = percentEncodedQuery
        }
        guard let componentsURL = components.url else {
            throw CouldNotInitializeURLRequest()
        }

        let request = URLRequest(url: componentsURL)
        session.dataTask(with: request) { data, response, error in
            completion(Result {
                if let error = error {
                    throw error
                } else if let data = data, let response = response as? HTTPURLResponse {
                    return (data, response)
                } else {
                    throw UnexpectedValuesRepresentation()
                }
            })
        }.resume()
    }
}

extension Dictionary {
    var isNotEmpty: Bool { return !self.isEmpty }
}
