//
//  RemoteBookLoader.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

public final class RemoteBookLoader: BookLoaderProtocol {
    public typealias Result = BookLoaderProtocol.Result
    private let url: URL
    private let client: HTTPClient

    public init(url: URL, client: HTTPClient) {
        self.url = url
        self.client = client
    }

    public enum Error: Swift.Error {
        case connectivity
        case invalidData
    }

    public func load(
        with term: String,
        countryCode: String? = nil,
        completion: @escaping (BookLoaderProtocol.Result) -> Void
    ) {
        var parameters: [String: String] = [:]
        if !term.isEmpty {
            parameters["term"] = term
        }
        if let country = countryCode {
            parameters["country"] = country
        }
        parameters["entity"]="ebook"

        do {
            try client.get(from: url, with: parameters) { [weak self] result in
                guard self != nil else { return }
                switch result {
                case .failure(let error):
                    print(error)
                    completion(.failure(RemoteBookLoader.Error.connectivity))
                case let .success((data, response)):
                    completion(RemoteBookLoader.map(data, from: response))
                }
            }
        } catch {
            completion(.failure(Error.connectivity))
        }
    }

    private static func map(_ data: Data, from response: HTTPURLResponse) -> Result {
        do {
            let books = try BooksMapper.map(data, from: response)
            return .success(books.toModels())
        } catch {
            return .failure(error)
        }
    }
}

private extension Array where Element == RemoteBook {
    func toModels() -> [Book] {
        return map {

            let bookImage = BookImage(
                smallImageUrl: $0.artworkUrl60,
                largeImageUrl: $0.artworkUrl100
            )

            let price = Price(
                price: $0.price,
                currency: $0.currency
            )

            let rating = Rating(
                userRating: $0.averageUserRating,
                userRatingCount: $0.userRatingCount
            )

            return         Book(
                identifier: $0.trackId ?? -1,
                title: $0.trackName,
                image: bookImage,
                description: $0.description,
                editor: $0.artistName,
                price: price,
                rating: rating,
                bookUrl: $0.trackViewUrl,
                releaseDate: $0.releaseDate?.toDate
            )
        }
    }
}
