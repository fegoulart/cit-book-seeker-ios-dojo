//
//  BooksMapper.swift
//  BookLoader
//
//  Created by Fernando Luiz Goulart on 11/01/22.
//  Copyright © 2022 com.leapi. All rights reserved.
//

import Foundation

final class BooksMapper {
    private struct Root: Decodable {
        let resultCount: Int
        let results: [RemoteBook]
    }

    private static var OK200: Int { return 200 }

    static func map(_ data: Data, from response: HTTPURLResponse) throws -> [RemoteBook] {
        guard response.statusCode == OK200,
            let root = try? JSONDecoder().decode(Root.self, from: data) else {
            throw RemoteBookLoader.Error.invalidData
        }

        return root.results
    }
}
