import ProjectDescription

let config = Config(
    generationOptions: [
        .enableCodeCoverage,
        .organizationName("com.goulart"),
        .disableAutogeneratedSchemes]
)
