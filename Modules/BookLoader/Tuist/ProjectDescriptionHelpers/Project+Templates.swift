import ProjectDescription

/// Project helpers are functions that simplify the way you define your project.
/// Share code to create targets, settings, dependencies,
/// Create your own conventions, e.g: a func that makes sure all shared targets are "static frameworks"
/// See https://docs.tuist.io/guides/helpers/

public extension Project {
    /// Helper function to create the Project for this Framework
    static func framework(
        name: String,
        platform: Platform,
        deploymentTarget: DeploymentTarget,
        packages: [Package] = [],
        appLibDependencies: [TargetDependency] = [],
        scripts: [TargetScript] = [],
        coreDataModels: [CoreDataModel] = []
    ) -> Project {

		let targets = makeFrameworkTargets(
            name: name,
            platform: platform,
            deploymentTarget: deploymentTarget,
            scripts: scripts,
            coreDataModels: coreDataModels
        )

		let schemes = makeFrameworkSchemes(name: name)

		return Project(
            name: name,
            organizationName: "Goulart",
            packages: packages,
            targets: targets,
            schemes: schemes
        )
    }

    // MARK: - Private

    /// Helper function to create a framework target and an associated unit test target
    private static func makeFrameworkTargets(
        name: String,
        platform: Platform,
        deploymentTarget: DeploymentTarget,
        scripts: [TargetScript] = [],
        coreDataModels: [CoreDataModel] = []
    ) -> [Target] {
        let sources = Target(
            name: name,
            platform: platform,
            product: .framework,
            bundleId: "com.goulart.\(name)",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["\(name)/Sources/**/*.swift"],
            resources: [],
			scripts: scripts,
            dependencies: [],
            coreDataModels: coreDataModels
        )
        let tests = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "com.goulart.\(name)Tests",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["\(name)/Tests/**"],
            resources: [],
            dependencies: [.target(name: name)]
        )
        return [sources, tests]
    }

    /// Helper function to create the application schemes
    private static func makeFrameworkSchemes(name: String) -> [Scheme] {
        let mainBuildAction: BuildAction = BuildAction(
            targets: [
                TargetReference(stringLiteral: "\(name)"),
                TargetReference(stringLiteral: "\(name)Tests")
            ]
        )

        let mainTestAction: TestAction =  TestAction.targets(
            [TestableTarget(target: "\(name)Tests", randomExecutionOrdering: true)],
            configuration: .debug,
            options: .options(coverage: true, codeCoverageTargets: [TargetReference(stringLiteral: "\(name)")]),
            diagnosticsOptions: [.mainThreadChecker]
        )

        let mainRunAction: RunAction = RunAction.runAction(configuration: .debug)

        let mainArchiveAction: ArchiveAction = ArchiveAction.archiveAction(configuration: .release)

        let mainProfileAction: ProfileAction = ProfileAction.profileAction(configuration: .release)

        let mainAnalyzeAction: AnalyzeAction = AnalyzeAction.analyzeAction(configuration: .debug)

        let mainScheme = Scheme(
            name: name,
            shared: true,
            buildAction: mainBuildAction,
            testAction: mainTestAction,
            runAction: mainRunAction,
            archiveAction: mainArchiveAction,
            profileAction: mainProfileAction,
            analyzeAction: mainAnalyzeAction
        )

        return [mainScheme]
    }
}
