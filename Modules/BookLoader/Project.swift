import ProjectDescription
import ProjectDescriptionHelpers

// MARK: - Project

// Creates our project using a helper function defined in ProjectDescriptionHelpers
let project = Project.framework(
    name: "BookLoader",
    platform: .iOS,
	deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
    packages: [],
    appLibDependencies: [],
	scripts: [.pre(path: "./scripts/swiftlint.sh", name: "SwiftLint")],
    coreDataModels: [
        CoreDataModel(
            .relativeToCurrentFile("./BookLoader/Sources/Book Cache/Infrastructure/CoreData/BookStore.xcdatamodeld"))
    ]
)
