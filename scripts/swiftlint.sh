if  [ "$CI" == "false" ]; 
then
    echo "CI don't run swiflint script"
elif which swiftlint >/dev/null; >/dev/null; 
then
    swiftlint
else
    echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
fi