import ProjectDescription
import ProjectDescriptionHelpers

// MARK: - Project

// Creates our project using a helper function defined in ProjectDescriptionHelpers
let project = Project.app(
    name: "BookSeeker",
    platform: .iOS,
    deploymentTarget: .iOS(targetVersion: "15.0", devices: [.iphone]),
    additionalTargets: [],
    packages: [],

    appLibDependencies: [
        .project(target: "BookLoader", path: "Modules/BookLoader")
    ],

    uiTestLibDependencies: [],

    unitTestLibDependencies: [],

    scripts: [.pre(path: "./scripts/swiftlint.sh", name: "SwiftLint")]
)
