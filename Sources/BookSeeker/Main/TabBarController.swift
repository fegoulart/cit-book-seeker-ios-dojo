import UIKit

enum TabbarItems: Int {
	case bookSearch
}

class TabbarController: UITabBarController {

	override func viewDidLoad() {
		super.viewDidLoad()
		configureTabbar()
	}

	func configureTabbar() {
		tabBar.backgroundColor = .systemGray6
		tabBar.barTintColor = .systemGray
		tabBar.tintColor = .black
		tabBar.unselectedItemTintColor = .black
	}
}
