//
//  TabBarCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit
import BookLoader

class TabBarCoordinatorsFactory {
	let tabBarController: UITabBarController

	private func tabBarItem(
		title: String?,
		image: UIImage?,
		selectedImage: UIImage?
	) -> UITabBarItem {

		let tabBarItem  = UITabBarItem(
			title: title,
			image: image,
			selectedImage: selectedImage
		)

		return tabBarItem
	}

    init(tabBarController: UITabBarController) {
        self.tabBarController = tabBarController
    }

	func searchCoordinator(searchTermLoader: PreviousSearchTermLoaderProtocol) -> BookSearchCoordinator {
		let tabItem = tabBarItem(
			title: nil,
			image: UIImage(systemName: "magnifyingglass"),
			selectedImage: UIImage(systemName: "magnifyingglass")
		)

		let coordinator = BookSearchCoordinator(
			tabBarController: tabBarController,
			item: tabItem,
			searchTermLoader: searchTermLoader
		)

		coordinator.start()
		return coordinator
	}
}
