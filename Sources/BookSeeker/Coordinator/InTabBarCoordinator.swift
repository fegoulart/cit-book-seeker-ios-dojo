//
//  InTabBarCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

protocol InTabBarCoordinator: Coordinator {
	var tabBarController: UITabBarController { get }
	var item: UITabBarItem { get }
}

extension InTabBarCoordinator {

	func addToTabbar(_ viewController: UIViewController) {
		viewController.view.backgroundColor = .white
		viewController.tabBarItem = item
		if tabBarController.viewControllers != nil {
			tabBarController.viewControllers?.append(viewController)
		} else {
			tabBarController.setViewControllers([viewController], animated: true)
		}
	}

}
