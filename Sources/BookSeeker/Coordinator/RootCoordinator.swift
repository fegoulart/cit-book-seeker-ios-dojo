//
//  RootCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public protocol RootCoordinator: Coordinator {
    var window: UIWindow { get }
}
