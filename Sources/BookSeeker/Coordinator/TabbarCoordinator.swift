//
//  TabBarCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit
import BookLoader

protocol TabbarCoordinatorDelegate: AnyObject {
	func switchToTab(item: TabbarItems)
}

final class TabbarCoordinator: RootCoordinator {

	unowned let window: UIWindow
	let searchTermLoader: PreviousSearchTermLoaderProtocol

	var currentViewController: UIViewController? {
		return nextCoordinator?.currentViewController
	}

	var nextCoordinator: Coordinator? {
		let current = self.coordinators[tabbarController.selectedIndex]
		return current
	}

	var tabbarController: TabbarController

	var coordinators: [InTabBarCoordinator]!

	init(
		window: UIWindow,
		tabbarController: TabbarController,
		searchTermLoader: PreviousSearchTermLoaderProtocol
	) {
        self.window = window
		self.tabbarController = tabbarController
		self.searchTermLoader = searchTermLoader
		self.coordinators = configureTabBarCoordinators()
	}

	func start(animated: Bool) {
		window.rootViewController = tabbarController
		tabbarController.configureTabbar()
	}

	func configureTabBarCoordinators() -> [InTabBarCoordinator] {
		let tabCoordinators = TabBarCoordinatorsFactory(tabBarController: tabbarController)

		let bookSearch = tabCoordinators.searchCoordinator(searchTermLoader: searchTermLoader)
		let coordinators: [InTabBarCoordinator] = [
			bookSearch
		]
		return coordinators
	}
}

extension TabbarCoordinator: TabbarCoordinatorDelegate {
	func switchToTab(item: TabbarItems) {
		tabbarController.selectedIndex = item.rawValue
	}
}
