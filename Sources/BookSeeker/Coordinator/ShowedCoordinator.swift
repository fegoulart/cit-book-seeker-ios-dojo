//
//  ShowedCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public protocol ShowedCoordinator: Coordinator {
	var currentViewController: UIViewController? { get set }
    func makeViewController() -> UIViewController
    func toUpdateIsNavigationBarHidden() -> Bool?
    func updateStatusBarVisibility()
    func show(viewController: UIViewController, animated: Bool)
}

public extension ShowedCoordinator {
    func start(animated: Bool = true) {
        let viewController = makeViewController()
        show(viewController: viewController, animated: animated)
        updateStatusBarVisibility()
    }

    func toUpdateIsNavigationBarHidden() -> Bool? {
        return nil
    }

    func updateStatusBarVisibility() {}
}
