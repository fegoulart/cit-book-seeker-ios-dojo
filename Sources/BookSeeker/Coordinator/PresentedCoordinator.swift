//
//  PresentedCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public protocol PresentedCoordinator: ShowedCoordinator {
    var presentingViewController: UIViewController { get }
}

public extension PresentedCoordinator {
    func show(viewController: UIViewController, animated: Bool) {
        currentViewController = viewController
        presentingViewController.present(viewController, animated: animated)
    }
}
