//
//  PushedCoodinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public protocol PushedCoordinator: ShowedCoordinator {
    var presentingViewController: UINavigationController { get }
}

public extension PushedCoordinator {
    func show(viewController: UIViewController, animated: Bool) {
        currentViewController = viewController
        presentingViewController.pushViewController(viewController, animated: animated)
    }

    func updateStatusBarVisibility() {
        if let updatedVisibility = toUpdateIsNavigationBarHidden() {
            presentingViewController.isNavigationBarHidden = updatedVisibility
        }
    }
}
