//
//  Coordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public protocol Coordinator: AnyObject {
    var nextCoordinator: Coordinator? { get }
    var currentViewController: UIViewController? { get }
    func start()
    func start(animated: Bool)
}

public extension Coordinator {
    func start() {
        start(animated: true)
    }
}
