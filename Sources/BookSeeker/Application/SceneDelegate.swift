//
//  SceneDelegate.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 16/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import os
import UIKit
import CoreData
import BookLoader

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow? {
      didSet {
        window?.overrideUserInterfaceStyle = .light
      }
    }

    private lazy var logger: Logger = {
        Logger(subsystem: "com.goulart.BookSeeker", category: "main")
    }()

    private lazy var httpClient: HTTPClient = {
        URLSessionHTTPClient(session: URLSession(configuration: .ephemeral))
    }()

    private lazy var bookStore: BookStoreProtocol = {
        do {
            return try CoreDataBookStore(
                storeURL: NSPersistentContainer
                    .defaultDirectoryURL()
                    .appendingPathComponent("book-store.sqlite"),
                bundle: Bundle.init(for: CoreDataBookStore.self))
        } catch {
            assertionFailure("Failed to instantiate CoreData store with error: \(error.localizedDescription)")
            logger.fault("Failed to instantiate CoreData store with error: \(error.localizedDescription)")
            return NullStore()
        }
    }()

    private lazy var localBookLoader: LocalBookLoader = {
        LocalBookLoader(store: bookStore, currentDate: Date.init)
    }()

    private var countryCode: String?

    convenience init(httpClient: HTTPClient, bookStore: BookStoreProtocol, countryCode: String? = nil) {
        self.init()
        self.httpClient = httpClient
        self.bookStore = bookStore
        self.countryCode = countryCode
    }

    lazy var coordinator: TabbarCoordinator = {
        return TabbarCoordinator(
            window: window!,
            tabbarController: TabbarController(),
            searchTermLoader: MainQueueDispatchDecorator(decoratee: localBookLoader)
        )
    }()

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let scene = (scene as? UIWindowScene) else { return }

        window = UIWindow(windowScene: scene)
		coordinator.start()
		window?.makeKeyAndVisible()
    }
}
