//
//  WebkitView.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 19/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit
import WebKit

final class WebKitView: WKWebView {

    override init(frame: CGRect = .zero, configuration: WKWebViewConfiguration) {
        super.init(frame: frame, configuration: configuration)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
