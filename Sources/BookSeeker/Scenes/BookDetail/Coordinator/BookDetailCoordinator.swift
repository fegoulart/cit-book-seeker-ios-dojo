//
//  BookDetailCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 19/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit
import BookLoader

final class BookDetailCoordinator: PushedCoordinator {
    var presentingViewController: UINavigationController
    var currentViewController: UIViewController?

    var nextCoordinator: Coordinator?
    var webviewURL: URL

    init(
        presentingViewController: UINavigationController,
        url: URL
    ) {
        self.presentingViewController = presentingViewController
        self.webviewURL = url
    }

    func makeViewController() -> UIViewController {
        return BookDetailViewController(url: webviewURL)
    }

}
import WebKit

public final class BookDetailViewController: UIViewController {
    private let url: URL
    let screen = WebKitView()

    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }

    public override func loadView() {
        view = self.screen
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.screen.load(URLRequest(url: self.url))
    }
}
