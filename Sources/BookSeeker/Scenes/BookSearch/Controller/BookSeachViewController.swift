//
//  BookSeachViewController.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit
#if DEBUG
import SwiftUI
#endif

public final class BookSearchViewController: UIViewController {

    private var previousSearchTermsTableController: PreviousSearchTermsTableController?
    private var searchTermBarController: SearchTermBarController?

    lazy var bookSearchView: BookSearchUIView = {
        guard
            let searchBar = searchTermBarController?.view,
            let table = previousSearchTermsTableController?.view
        else {
            return BookSearchUIView()
        }
        var view: BookSearchUIView = BookSearchUIView(searchBar: searchBar, searchTermTable: table)
        table.delegate = previousSearchTermsTableController
        table.dataSource = previousSearchTermsTableController
        return view
    }()

    public convenience init(
        previousSearchTermsTableController: PreviousSearchTermsTableController,
        searchTermBarController: SearchTermBarController
    ) {
        self.init()
        self.previousSearchTermsTableController = previousSearchTermsTableController
        self.searchTermBarController = searchTermBarController
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
    }

    public override func loadView() {
        super.loadView()
        view = bookSearchView
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.previousSearchTermsTableController?.refresh()
    }
}

#if DEBUG
struct BookSearchViewController_Previews: PreviewProvider {
    static var previews: some View {
        ViewControllerPreview {
            BookSearchViewController()
        }
    }
}
#endif
