//
//  PreviousSearchTermsTableController.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation
import UIKit

public final class PreviousSearchTermsTableController: NSObject, UITableViewDelegate, UITableViewDataSource {

    lazy var view: UITableView = {
                let tableView = UITableView()
                tableView.separatorStyle = .singleLine
                tableView.tableFooterView = UIView()
                tableView.dataSource = self
                tableView.delegate = self
                return tableView
    }()

    var tableModel = [SearchTermCellController]() {
        didSet { view.reloadData() }
    }

    weak var navigationDelegate: BookSearchNavigationDelegate?
    var viewModel: BookSearchViewModel

    public init(viewModel: BookSearchViewModel) {
        self.viewModel = viewModel
        super.init()
        self.view.delegate = self
        self.view.dataSource = self
    }

    @objc func refresh() {
        viewModel.loadPreviousSearchTerms()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModel.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return cellController(forRowAt: indexPath).view()
    }

    private func cellController(forRowAt indexPath: IndexPath) -> SearchTermCellController {
        return tableModel[indexPath.row]
    }

    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        navigationDelegate?.navigateToBookListing(searchTerm: tableModel[indexPath.row].getSearchTerm())
        return indexPath
    }
}
