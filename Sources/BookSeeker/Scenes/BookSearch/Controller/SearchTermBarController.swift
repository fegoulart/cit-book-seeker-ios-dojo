//
//  SearchTermBarController.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation
import UIKit

public final class SearchTermBarController: NSObject, UISearchBarDelegate {
    lazy var view: UISearchBar = {
        let search = UISearchBar()
        search.delegate = self
        search.backgroundImage = UIImage()
        search.searchTextField.autocapitalizationType = UITextAutocapitalizationType.none
        search.searchTextField.addTarget(
            self,
            action: #selector(
                self.textFieldDidChange(_:)
            ),
            for: UIControl.Event.editingChanged
        )
        return search
    }()

    weak var navigationDelegate: BookSearchNavigationDelegate?

    override init() {
        super.init()
    }

    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchTerm = view.text else { return }
        guard let mDelegate = navigationDelegate else {
            return
        }
        mDelegate.navigateToBookListing(searchTerm: searchTerm)
    }

    @objc func textFieldDidChange(_ textfield: UITextField) {
            if let text: String = textfield.text {
                self.view.searchTextField.text = text.lowercased()
            }
    }
}
