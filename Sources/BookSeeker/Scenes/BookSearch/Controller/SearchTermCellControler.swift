//
//  SearchTermCellControler.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

public final class SearchTermCellController {
    private let viewModel: SearchTermViewModel

    public init(viewModel: SearchTermViewModel) {
        self.viewModel = viewModel
    }

    public func getSearchTerm() -> String {
        return viewModel.searchTerm
    }

    public func view() -> UITableViewCell {
        return newCell()
    }

    private func newCell() -> SearchTermCell {
        let mCell = binded(
            SearchTermCell(
                style: .default,
                reuseIdentifier: String(
                    describing: SearchTermCell.self
                )
            )
        )
        return mCell
    }

    private func binded(_ cell: SearchTermCell) -> SearchTermCell {
        var content = UIListContentConfiguration.cell()
        content.text = viewModel.searchTerm
        cell.contentConfiguration = content
        return cell
    }
}
