//
//  SearchTermViewModel.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation

public final class SearchTermViewModel {
    typealias Observer<T> = (T) -> Void

    private let model: String

    public init(model: String) {
        self.model = model
    }

    public var searchTerm: String {
        return model
    }
}
