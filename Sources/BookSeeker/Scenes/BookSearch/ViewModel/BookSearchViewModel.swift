//
//  BookSearchViewModel.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import Foundation
import BookLoader

public final class BookSearchViewModel {
    typealias Observer<T> = (T) -> Void

    private var countryCode: String?
    private var searchTermLoader: PreviousSearchTermLoaderProtocol

    init(searchTermLoader: PreviousSearchTermLoaderProtocol, countryCode: String? = nil) {
        self.searchTermLoader = searchTermLoader
        self.countryCode = countryCode
    }

    var onLoadingStateChange: Observer<Bool>?
    var onPreviousSearchTermLoad: Observer<[String]>?

    func loadPreviousSearchTerms() {
        onLoadingStateChange?(true)

        searchTermLoader.loadPreviousSearchTerms(countryCode: self.countryCode) { [weak self] result in
            if let searchTerms = try? result.get() {
                self?.onPreviousSearchTermLoad?(searchTerms)
            }
            self?.onLoadingStateChange?(false)
        }
    }
}
