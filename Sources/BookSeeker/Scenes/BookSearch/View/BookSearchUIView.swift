//
//  BookSearchUIView.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 16/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import UIKit

public final class BookSearchUIView: UIView {

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Search"

        guard let nyFont = UIFont(name: "NewYork-Bold", size: UIFont.labelFontSize) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        label.font = UIFontMetrics.default.scaledFont(for: nyFont).withSize(54.0)
        label.adjustsFontForContentSizeCategory = true
        return label
    }()

    private var searchBar: UISearchBar?
    private var searchTermTable: UITableView?

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }

    public convenience init(searchBar: UISearchBar, searchTermTable: UITableView) {
        self.init()
        self.searchBar = searchBar
        self.searchTermTable = searchTermTable
        buildView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BookSearchUIView: ViewCoding {
    public func buildViewHierarchy() {
        addSubview(titleLabel)
        guard let mSearchBar = searchBar, let mSearchTermTable = searchTermTable else { return }
        addSubview(mSearchBar)
        addSubview(mSearchTermTable)
    }

    public func setupConstraints() {
        titleLabel.anchors.top.spacing(20, to: self.safeAreaLayoutGuide.anchors.top)
        titleLabel.anchors.edges.pin(insets: 20, axis: .horizontal, alignment: .leading)
        guard let mSearchBar = searchBar, let mSearchTermTable = searchTermTable else { return }
        titleLabel.anchors.bottom.spacing(20, to: mSearchBar.anchors.top)
        mSearchBar.anchors.edges.pin(insets: 20, axis: .horizontal, alignment: .center)
        mSearchBar.anchors.bottom.spacing(20, to: mSearchTermTable.anchors.top)
        mSearchTermTable.anchors.edges.pin(insets: 20, axis: .horizontal, alignment: .center)
        mSearchTermTable.anchors.left.pin(inset: 20)
        mSearchTermTable.anchors.right.pin(inset: 20)
        mSearchTermTable.anchors.height.equal(self.safeAreaLayoutGuide.anchors.height.offsetting(by: -100))
    }

    public func additionalSetup() {
        backgroundColor = .white
    }
}
