//
//  SearchTermCell.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

public final class SearchTermCell: UITableViewCell {
    public let searchTermLabel = UILabel()
}
