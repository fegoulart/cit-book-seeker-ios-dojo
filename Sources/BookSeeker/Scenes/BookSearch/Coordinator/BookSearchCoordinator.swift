//
//  BookSearchCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import os
import UIKit
import BookLoader
import CoreData

protocol BookSearchNavigationDelegate: AnyObject {
    func navigateToBookListing(searchTerm: String)
}

final class BookSearchCoordinator: InTabBarCoordinator {
    unowned let tabBarController: UITabBarController
    unowned let item: UITabBarItem
    var nextCoordinator: Coordinator?
    var currentViewController: UIViewController?
    var searchTermLoader: PreviousSearchTermLoaderProtocol
    var countryCode: String?

    private lazy var logger: Logger = {
        Logger(subsystem: "com.goulart.BookSeeker", category: "main")
    }()

    private lazy var httpClient: HTTPClient = {
        URLSessionHTTPClient(session: URLSession(configuration: .ephemeral))
    }()

    private lazy var bookStore: BookStoreProtocol = {
        do {
            return try CoreDataBookStore(
                storeURL: NSPersistentContainer
                    .defaultDirectoryURL()
                    .appendingPathComponent("book-store.sqlite"),
                bundle: Bundle.init(for: CoreDataBookStore.self))
        } catch {
            assertionFailure("Failed to instantiate CoreData store with error: \(error.localizedDescription)")
            logger.fault("Failed to instantiate CoreData store with error: \(error.localizedDescription)")
            return NullStore()
        }
    }()

    private lazy var remoteBookLoader = {
        return RemoteBookLoader(
            url: URL(string: "https://itunes.apple.com/search")!,
            client: httpClient
        )
    }()

    private lazy var localBookLoader: LocalBookLoader = {
        LocalBookLoader(store: bookStore, currentDate: Date.init)
    }()

    private lazy var bookLoader: BookLoaderProtocol = {
        return BookLoaderWithFallbackComposite(
            primary: BookLoaderCacheDecorator(
                decoratee: remoteBookLoader,
                cache: localBookLoader),
            fallback: localBookLoader
        )
    }()

    init(
        tabBarController: UITabBarController,
        item: UITabBarItem,
        searchTermLoader: PreviousSearchTermLoaderProtocol,
        countryCode: String? = nil
    ) {
        self.tabBarController = tabBarController
        self.item = item
        self.searchTermLoader = searchTermLoader
        self.countryCode = countryCode
    }

    func start(animated: Bool = true) {
        let controller = makeNavWithViewController()
        addToTabbar(controller)
        currentViewController = controller
    }

    private func makeNavWithViewController() -> UINavigationController {
        let bookSearchViewController = makeBookSearchViewController()
        let navigationController = UINavigationController(rootViewController: bookSearchViewController)
        return navigationController
    }

    private func makeBookSearchViewController() -> BookSearchViewController {
        let previousSearchTermsTableController = makePreviousSearchTermsController()
        let searchTermBarController = makeSearchTermBarController()
        let bookSearchViewController = BookSearchViewController(
            previousSearchTermsTableController: previousSearchTermsTableController,
            searchTermBarController: searchTermBarController
        )
        return bookSearchViewController
    }

    private func makePreviousSearchTermsController() -> PreviousSearchTermsTableController {
        let bookSearchViewModel = BookSearchViewModel(searchTermLoader: searchTermLoader, countryCode: countryCode)
        let previousSearchTermsTableController = PreviousSearchTermsTableController(viewModel: bookSearchViewModel)
        previousSearchTermsTableController.navigationDelegate = self
        bookSearchViewModel.onPreviousSearchTermLoad = BookSearchCoordinator.adaptSearchTermToCellControllers(
            forwardingTo: previousSearchTermsTableController
        )
        return previousSearchTermsTableController
    }

    private func makeSearchTermBarController() -> SearchTermBarController {
        let searchTermBarController = SearchTermBarController()
        searchTermBarController.navigationDelegate = self
        return searchTermBarController
    }

    private static func adaptSearchTermToCellControllers(
        forwardingTo controller: PreviousSearchTermsTableController
    ) -> ([String]) -> Void {
        return { [weak controller] searchTerm in
            controller?.tableModel = searchTerm.map { model in
                SearchTermCellController(viewModel: SearchTermViewModel(model: model))
            }

        }
    }
}

extension BookSearchCoordinator: BookSearchNavigationDelegate {
    func navigateToBookListing(searchTerm: String) {
        guard let currentViewController = currentViewController as? UINavigationController else { return }

        let coordinator = BookListingCoordinator(
            presentingViewController: currentViewController,
            bookLoader: MainQueueDispatchDecorator(decoratee: bookLoader),
            imageLoader: RemoteBookImageDataLoader(client: httpClient),
            searchTerm: searchTerm
        )
        nextCoordinator = coordinator
        nextCoordinator?.start()
    }

}
