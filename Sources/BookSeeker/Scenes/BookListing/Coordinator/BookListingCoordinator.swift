//
//  BookListingCoordinator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 17/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit
import BookLoader

protocol BookListingNavigationDelegate: AnyObject {
    func navigateToBookDetail(url: URL)
}

final class BookListingCoordinator: PushedCoordinator {

    var presentingViewController: UINavigationController
    var nextCoordinator: Coordinator?
    var currentViewController: UIViewController?
    var bookLoader: BookLoaderProtocol
    var imageLoader: BookImageDataLoader
    var searchTerm: String
    var countryCode: String?

    init(
        presentingViewController: UINavigationController,
        bookLoader: BookLoaderProtocol,
        imageLoader: BookImageDataLoader,
        searchTerm: String,
        countryCode: String? = nil
    ) {
        self.presentingViewController = presentingViewController
        self.bookLoader = bookLoader
        self.imageLoader = imageLoader
        self.searchTerm = searchTerm
        self.countryCode = countryCode
    }

    func makeViewController() -> UIViewController {
        let (bookViewModel, refreshController) = makeBookRefreshViewController()
        let viewController: BookViewController = BookViewController(
            refreshController: refreshController
        )
        viewController.navigationDelegate = self
        bookViewModel.onBookLoad = BookListingCoordinator.adaptBookToCellControllers(
            forwardingTo: viewController,
            loader: MainQueueDispatchDecorator(decoratee: imageLoader)
        )
        currentViewController = viewController
        return viewController
    }

    private func makeBookRefreshViewController() -> (BookViewModel, BookRefreshViewController) {
        let bookViewModel = BookViewModel(
            searchTerm: searchTerm,
            bookLoader: bookLoader,
            countryCode: countryCode
        )
        let refreshController = BookRefreshViewController(
            viewModel: bookViewModel
        )
        return (bookViewModel, refreshController)
    }

    private static func adaptBookToCellControllers(
        forwardingTo controller: BookViewController,
        loader: BookImageDataLoader
    ) -> ([Book]) -> Void {
        return { [weak controller] searchTerm in

            guard searchTerm.count > 0  else {
                self.showNoResultAlert(controller)
                return
            }

            controller?.tableModel = searchTerm.map { model in
                BookImageCellController(
                    viewModel: BookImageViewModel(
                        model: model,
                        imageLoader: loader,
                        imageTransformer: { imageData in
                            guard let image = UIImage.init(data: imageData) else { return UIImage() }
                            return image.scaled(targetWidth: 60)
                        }
                    )
                )
            }
        }
    }

    private static func showNoResultAlert(_ controller: BookViewController?) {
        guard let controller = controller else { return }
        let alert = UIAlertController(title: "Alert", message: "No results found", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
            case .default:
                print("default")

            case .cancel:
                print("cancel")

            case .destructive:
                print("destructive")

            @unknown default:
                fatalError()
            }
        }))
        controller.present(alert, animated: true, completion: nil)
    }
}

extension BookListingCoordinator: BookListingNavigationDelegate {
    func navigateToBookDetail(url: URL) {
        guard let currentViewController = currentViewController?.navigationController else { return }
        let coordinator = BookDetailCoordinator(
            presentingViewController: currentViewController,
            url: url
        )
        nextCoordinator = coordinator
        nextCoordinator?.start()
    }
}
