//
//  BookViewController.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

public final class BookViewController: UITableViewController, UITableViewDataSourcePrefetching {
    private var refreshController: BookRefreshViewController?
    var tableModel = [BookImageCellController]() {
        didSet {
            tableView.reloadData()
            tableView.invalidateIntrinsicContentSize()
        }
    }
    weak var navigationDelegate: BookListingNavigationDelegate?

    convenience init(refreshController: BookRefreshViewController) {
        self.init()
        self.refreshController = refreshController
        tableView.register(BookImageCell.self, forCellReuseIdentifier: String(describing: BookImageCell.self))
        tableView.rowHeight = 120
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 25
        tableView.prefetchDataSource = self
        refreshControl = refreshController?.view
        refreshController?.refresh()
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModel.count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellController(forRowAt: indexPath).view(tableView: tableView)
    }

    public override func tableView(
        _ tableView: UITableView,
        didEndDisplaying cell: UITableViewCell,
        forRowAt indexPath: IndexPath
    ) {
        cancelCellControllerLoad(forRowAt: indexPath)
    }

    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { indexPath in
            cellController(forRowAt: indexPath).preload()
        }
    }

    public func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach(cancelCellControllerLoad)
    }

    private func cellController(forRowAt indexPath: IndexPath) -> BookImageCellController {
        return tableModel[indexPath.row]
    }

    private func cancelCellControllerLoad(forRowAt indexPath: IndexPath) {
        cellController(forRowAt: indexPath).cancelLoad()
    }

    public override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard let bookUrl = tableModel[indexPath.row].getUrl(), let mURL = URL(string: bookUrl) else {
            return nil
        }
        navigationDelegate?.navigateToBookDetail(url: mURL)
        return nil
    }
}
