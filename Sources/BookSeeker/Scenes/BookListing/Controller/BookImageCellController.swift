//
//  BookImageCellController.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

final class BookImageCellController {
    private let viewModel: BookImageViewModel<UIImage>

    init(viewModel: BookImageViewModel<UIImage>) {
        self.viewModel = viewModel
    }

    func view(tableView: UITableView) -> UITableViewCell {
        return newCell()
    }

    private func newCell() -> BookImageCell {
        let mCell = binded(
            BookImageCell(
                style: .subtitle,
                reuseIdentifier: String(describing: BookImageCell.self)
            )
        )
        viewModel.loadImageData()
        return mCell
    }

    func preload() {
        viewModel.loadImageData()
    }

    func cancelLoad() {
        viewModel.cancelImageDataLoad()
    }

    func getUrl() -> String? {
        return viewModel.bookUrl
    }

    private func binded(_ cell: BookImageCell) -> BookImageCell {

        var content = UIListContentConfiguration.subtitleCell()

        content.text = viewModel.title
        content.secondaryText = viewModel.editor
        cell.contentConfiguration = content
        viewModel.onImageLoad = { [weak cell] image in
            guard let mCell = cell else {
                return
            }
            content.image = image
            mCell.contentConfiguration = content
        }
        viewModel.onImageLoadingStateChange = { [weak cell] isLoading in
            cell?.bookImageContainer.isShimmering = isLoading
        }

        return cell
    }
}
