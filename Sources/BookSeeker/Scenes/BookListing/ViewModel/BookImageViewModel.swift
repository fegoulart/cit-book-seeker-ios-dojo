//
//  BookImageViewModel.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation
import BookLoader

final class BookImageViewModel<Image> {
    typealias Observer<T> = (T) -> Void

    private var task: BookImageDataLoaderTask?
    private let model: Book
    private let imageLoader: BookImageDataLoader
    private let imageTransformer: (Data) -> Image?

    init(model: Book, imageLoader: BookImageDataLoader, imageTransformer: @escaping (Data) -> Image?) {
        self.model = model
        self.imageLoader = imageLoader
        self.imageTransformer = imageTransformer
    }

    var title: String? {
        return model.title
    }

    var editor: String? {
        return model.editor
    }

    var bookUrl: String? {
        return model.bookUrl
    }

    var onImageLoad: Observer<Image>?
    var onImageLoadingStateChange: Observer<Bool>?
    var onShouldRetryImageLoadStateChange: Observer<Bool>?

    func loadImageData() {
        guard
            let stringUrl = model.image?.largeImageUrl,
            let imageUrl: URL = URL(string: stringUrl) else {
                return
            }
        onImageLoadingStateChange?(true)
        onShouldRetryImageLoadStateChange?(false)
        task = imageLoader.loadImageData(from: imageUrl) { [weak self] result in
            self?.handle(result)
        }
    }

    private func handle(_ result: BookImageDataLoader.Result) {
        if let image = (try? result.get()).flatMap(imageTransformer) {
            onImageLoad?(image)
        }
        onImageLoadingStateChange?(false)
    }

    func cancelImageDataLoad() {
        task?.cancel()
        task = nil
    }
}
