//
//  BookViewModel.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation
import BookLoader

final class BookViewModel {
    typealias Observer<T> = (T) -> Void

    private let bookLoader: BookLoaderProtocol
    private let searchTerm: String
    private let countryCode: String?

    init(
        searchTerm: String,
        bookLoader: BookLoaderProtocol,
        countryCode: String?
    ) {
        self.searchTerm = searchTerm
        self.bookLoader = bookLoader
        self.countryCode = countryCode
    }

    var onLoadingStateChange: Observer<Bool>?
    var onBookLoad: Observer<[Book]>?

    func loadBook() {
        onLoadingStateChange?(true)
        bookLoader.load(
            with: self.searchTerm,
            countryCode: self.countryCode
        ) { [weak self] result in
            if let book = try? result.get() {
                self?.onBookLoad?(book)
            }
            self?.onLoadingStateChange?(false)
        }
    }
}
