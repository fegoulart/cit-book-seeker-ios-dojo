//
//  BookImageCell.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

public final class BookImageCell: UITableViewCell {
    public let titleLabel = UILabel()
    public let editorLabel = UILabel()
    public let bookImageContainer = UIView()
    public let bookImageView = UIImageView()
}
