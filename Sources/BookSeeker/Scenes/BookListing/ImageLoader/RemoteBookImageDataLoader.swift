//
//  RemoteBookImageDataLoader.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation
import BookLoader

public final class RemoteBookImageDataLoader: BookImageDataLoader {
    private let client: HTTPClient

    public init(client: HTTPClient) {
        self.client = client
    }

    public enum Error: Swift.Error {
        case connectivity
        case invalidData
    }

    private final class HTTPClientTaskWrapper: BookImageDataLoaderTask {
        private var completion: ((BookImageDataLoader.Result) -> Void)?

        var wrapped: HTTPClientTask?

        init(_ completion: @escaping (BookImageDataLoader.Result) -> Void) {
            self.completion = completion
        }

        func complete(with result: BookImageDataLoader.Result) {
            completion?(result)
        }

        func cancel() {
            preventFurtherCompletions()
            wrapped?.cancel()
        }

        private func preventFurtherCompletions() {
            completion = nil
        }
    }

    public func loadImageData(
        from url: URL,
        completion: @escaping (BookImageDataLoader.Result) -> Void
    ) -> BookImageDataLoaderTask {
        let task = HTTPClientTaskWrapper(completion)
        task.wrapped = try? client.get(from: url, with: [:]) { [weak self] result in
            guard self != nil else { return }

            task.complete(with: result
                .mapError { _ in Error.connectivity }
                .flatMap { (data, response) in
                    let isValidResponse = response.statusCode == 200 && !data.isEmpty
                    return isValidResponse ? .success(data) : .failure(Error.invalidData)
                })
        } as? HTTPClientTask
        return task
    }
}
