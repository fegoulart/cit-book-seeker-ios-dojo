//
//  BookImageDataLoader.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import Foundation

public protocol BookImageDataLoaderTask {
    func cancel()
}

public protocol BookImageDataLoader {
    typealias Result = Swift.Result<Data, Error>

    func loadImageData(from url: URL, completion: @escaping (Result) -> Void) -> BookImageDataLoaderTask
}
