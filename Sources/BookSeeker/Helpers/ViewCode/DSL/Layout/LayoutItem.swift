import UIKit

public protocol LayoutItem {
    var superview: UIView? { get }
}

public extension LayoutItem {
    @nonobjc var anchors: LayoutAnchors<Self> { LayoutAnchors(base: self) }
}
