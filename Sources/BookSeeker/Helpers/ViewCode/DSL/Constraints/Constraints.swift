import UIKit

public final class Constraints: Collection {
    public typealias Element = NSLayoutConstraint
    public typealias Index = Int

    public subscript(position: Int) -> NSLayoutConstraint {
        return constraints[position]
    }
    public var startIndex: Int { constraints.startIndex }
    public var endIndex: Int { constraints.endIndex }
    // swiftlint:disable identifier_name
    public func index(after i: Int) -> Int { i + 1 }
    // swiftlint:enable identifier_name

    /// Returns all of the created constraints.
    public private(set) var constraints = [NSLayoutConstraint]()

    /// All of the constraints created in the given closure are automatically
    /// activated at the same time. This is more efficient then installing them
    /// one-be-one. More importantly, it allows to make changes to the constraints
    /// before they are installed (e.g. change `priority`).
    ///
    /// - parameter activate: Set to `false` to disable automatic activation of
    /// constraints.
    @discardableResult public init(activate: Bool = true, _ closure: () -> Void) {
        Constraints.stack.append(self)
        closure() // create constraints
        Constraints.stack.removeLast()
        if activate { NSLayoutConstraint.activate(constraints) }
    }

    // MARK: Activate
    /// Activates each constraint in the reciever.
    public func activate() {
        NSLayoutConstraint.activate(constraints)
    }

    /// Deactivates each constraint in the reciever.
    public func deactivate() {
        NSLayoutConstraint.deactivate(constraints)
    }

    // MARK: Adding Constraints
    /// Creates and automatically installs a constraint.
    static func add(
        item item1: Any,
        attribute attr1: NSLayoutConstraint.Attribute,
        relatedBy relation: NSLayoutConstraint.Relation = .equal,
        toItem item2: Any? = nil,
        attribute attr2: NSLayoutConstraint.Attribute? = nil,
        multiplier: CGFloat = 1,
        constant: CGFloat = 0) -> NSLayoutConstraint {
        precondition(Thread.isMainThread, "Align APIs can only be used from the main thread")

        (item1 as? UIView)?.translatesAutoresizingMaskIntoConstraints = false

        let constraint = NSLayoutConstraint(
            item: item1,
            attribute: attr1,
            relatedBy: relation,
            toItem: item2,
            attribute: attr2 ?? .notAnAttribute,
            multiplier: multiplier,
            constant: constant
        )

        install(constraint)

        return constraint
    }

    /// Creates and automatically installs a constraint between two anchors.
    static func add<T1, A1, T2, A2>(
        _ lhs: Anchor<T1, A1>,
        _ rhs: Anchor<T2, A2>,
        constant: CGFloat = 0,
        multiplier: CGFloat = 1,
        relation: NSLayoutConstraint.Relation = .equal
    ) -> NSLayoutConstraint {
        add(
            item: lhs.item,
            attribute: lhs.attribute,
            relatedBy: relation,
            toItem: rhs.item,
            attribute: rhs.attribute,
            multiplier: (multiplier / lhs.multiplier) * rhs.multiplier,
            constant: constant - lhs.offset + rhs.offset
        )
    }

    /// Creates and automatically installs a constraint between an anchor and
    /// a given item.
    static func add<T1, A1>(
        _ lhs: Anchor<T1, A1>,
        toItem item2: Any?,
        attribute attr2: NSLayoutConstraint.Attribute?,
        constant: CGFloat = 0,
        multiplier: CGFloat = 1,
        relation: NSLayoutConstraint.Relation = .equal
    ) -> NSLayoutConstraint {
        add(
            item: lhs.item,
            attribute: lhs.attribute,
            relatedBy: relation,
            toItem: item2,
            attribute: attr2,
            multiplier: multiplier / lhs.multiplier,
            constant: constant - lhs.offset
        )
    }

    private static var stack = [Constraints]() // this is what enabled constraint auto-installing

    private static func install(_ constraint: NSLayoutConstraint) {
        if let group = stack.last {
            group.constraints.append(constraint)
        } else {
            constraint.isActive = true
        }
    }
}
// swiftlint:disable identifier_name
public extension Constraints {
    @discardableResult convenience init<A: LayoutItem>(for a: A, _ closure: (LayoutAnchors<A>) -> Void) {
        self.init { closure(a.anchors) }
    }

    @discardableResult convenience init<A: LayoutItem, B: LayoutItem>(
        for a: A,
        _ b: B,
        _ closure: (LayoutAnchors<A>, LayoutAnchors<B>
        ) -> Void) {
        self.init { closure(a.anchors, b.anchors) }
    }

    @discardableResult convenience init<A: LayoutItem, B: LayoutItem, C: LayoutItem>(
        for a: A,
        _ b: B,
        _ c: C,
        _ closure: (LayoutAnchors<A>, LayoutAnchors<B>, LayoutAnchors<C>) -> Void
    ) {
        self.init { closure(a.anchors, b.anchors, c.anchors) }
    }

    @discardableResult convenience init<A: LayoutItem, B: LayoutItem, C: LayoutItem, D: LayoutItem>(
        for a: A,
        _ b: B,
        _ c: C,
        _ d: D,
        _ closure: (LayoutAnchors<A>, LayoutAnchors<B>, LayoutAnchors<C>, LayoutAnchors<D>) -> Void
    ) {
        self.init { closure(a.anchors, b.anchors, c.anchors, d.anchors) }
    }
}
// swiftlint:enable identifier_name
