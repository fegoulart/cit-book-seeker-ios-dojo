import UIKit

public struct AnchorCollectionEdges {
    let item: LayoutItem
    var isAbsolute = false

    // By default, edges use locale-specific `.leading` and `.trailing`
    public func absolute() -> AnchorCollectionEdges {
        AnchorCollectionEdges(item: item, isAbsolute: true)
    }

    public typealias Axis = NSLayoutConstraint.Axis

    // MARK: Core API
    @discardableResult public func equal(_ item2: LayoutItem, insets: EdgeInsets = .zero) -> [NSLayoutConstraint] {
        pin(to: item2, insets: insets)
    }

    @discardableResult public func lessThanOrEqual(
        _ item2: LayoutItem,
        insets: EdgeInsets = .zero
    ) -> [NSLayoutConstraint] {
        pin(to: item2, insets: insets, axis: nil, alignment: .center, isCenteringEnabled: false)
    }

    @discardableResult public func equal(_ item2: LayoutItem, insets: CGFloat) -> [NSLayoutConstraint] {
        pin(to: item2, insets: EdgeInsets(top: insets, left: insets, bottom: insets, right: insets))
    }

    @discardableResult public func lessThanOrEqual(_ item2: LayoutItem, insets: CGFloat) -> [NSLayoutConstraint] {
        pin(
            to: item2,
            insets: EdgeInsets(
                top: insets,
                left: insets,
                bottom: insets,
                right: insets
            ),
            axis: nil,
            alignment: .center,
            isCenteringEnabled: false
        )
    }

    // MARK: Semantic API
    /// Pins the edges to the edges of the given item. By default, pins the edges
    /// to the superview.
    ///
    /// - parameter target: The target view, by default, uses the superview.
    /// - parameter insets: Insets the reciever's edges by the given insets.
    /// - parameter axis: If provided, creates constraints only along the given
    /// axis. For example, if you pass axis `.horizontal`, only the `.leading`,
    /// `.trailing` (and `.centerX` if needed) attributes are used. `nil` by default
    /// - parameter alignment: `.fill` by default, see `Alignment` for a list of
    /// the available options.
    @discardableResult public func pin(
        to item2: LayoutItem? = nil,
        insets: CGFloat,
        axis: Axis? = nil,
        alignment: Alignment = .fill
    ) -> [NSLayoutConstraint] {
        pin(
            to: item2,
            insets: EdgeInsets(
                top: insets,
                left: insets,
                bottom: insets,
                right: insets),
            axis: axis,
            alignment: alignment
        )
    }

    /// Pins the edges to the edges of the given item. By default, pins the edges
    /// to the superview.
    ///
    /// - parameter target: The target view, by default, uses the superview.
    /// - parameter insets: Insets the reciever's edges by the given insets.
    /// - parameter axis: If provided, creates constraints only along the given
    /// axis. For example, if you pass axis `.horizontal`, only the `.leading`,
    /// `.trailing` (and `.centerX` if needed) attributes are used. `nil` by default
    /// - parameter alignment: `.fill` by default, see `Alignment` for a list of
    /// the available options.
    @discardableResult public func pin(
        to item2: LayoutItem? = nil,
        insets: EdgeInsets = .zero,
        axis: Axis? = nil,
        alignment: Alignment = .fill
    ) -> [NSLayoutConstraint] {
        pin(
            to: item2,
            insets: insets,
            axis: axis,
            alignment: alignment,
            isCenteringEnabled: true
        )
    }
    // swiftlint:disable function_body_length
    private func pin(
        to item2: LayoutItem?,
        insets: EdgeInsets,
        axis: Axis?,
        alignment: Alignment,
        isCenteringEnabled: Bool
    ) -> [NSLayoutConstraint] {
        let item2 = item2 ?? item.superview!
        let left: NSLayoutConstraint.Attribute = isAbsolute ? .left : .leading
        let right: NSLayoutConstraint.Attribute = isAbsolute ? .right : .trailing
        var constraints = [NSLayoutConstraint]()

        func constrain(
            attribute: NSLayoutConstraint.Attribute,
            relation: NSLayoutConstraint.Relation, constant: CGFloat
        ) {
            constraints.append(
                Constraints.add(
                    item: item,
                    attribute: attribute,
                    relatedBy: relation,
                    toItem: item2,
                    attribute: attribute,
                    multiplier: 1,
                    constant: constant
                )
            )
        }

        if axis == nil || axis == .horizontal {
            constrain(
                attribute: left,
                relation:
                    alignment.horizontal == .fill ||
                alignment.horizontal == .leading ? .equal : .greaterThanOrEqual,
                constant: insets.left
            )
            constrain(
                attribute: right,
                relation:
                    alignment.horizontal == .fill ||
                alignment.horizontal == .trailing ? .equal : .lessThanOrEqual,
                constant: -insets.right
            )
            if alignment.horizontal == .center && isCenteringEnabled {
                constrain(
                    attribute: .centerX,
                    relation: .equal,
                    constant: 0
                )
            }
        }

        if axis == nil || axis == .vertical {
            constrain(
                attribute: .top,
                relation: alignment.vertical == .fill || alignment.vertical == .top ? .equal : .greaterThanOrEqual,
                constant: insets.top
            )
            constrain(
                attribute: .bottom,
                relation: alignment.vertical == .fill || alignment.vertical == .bottom ? .equal : .lessThanOrEqual,
                constant: -insets.bottom
            )
            if alignment.vertical == .center && isCenteringEnabled {
                constrain(
                    attribute: .centerY,
                    relation: .equal,
                    constant: 0
                )
            }
        }
        return constraints
    }
    // swiftlint:enable function_body_length
}
