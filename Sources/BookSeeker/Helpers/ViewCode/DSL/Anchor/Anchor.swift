import UIKit

/// An anchor represents one of the view's layout attributes (e.g. `left`,
/// `centerX`, `width`, etc).
///
/// Instead of creating `NSLayoutConstraint` objects directly, start with a `UIView`,
/// or `UILayoutGuide` object you wish to constrain, and select one of
/// that object’s anchor properties. These properties correspond to the main
/// `NSLayoutConstraint.Attribute` values used in Auto Layout, and provide an
/// appropriate `Anchor` type for creating constraints to that attribute. For
/// example, `view.anchors.top` is represted by `Anchor<AnchorType.Edge, AnchorAxis.Vertical>`.
/// Use the anchor’s methods to construct your constraint.
///
/// - note: `UIView` does not provide anchor properties for the layout margin attributes.
/// Instead, the `layoutMarginsGuide` property provides a `UILayoutGuide` object that
/// represents these margins. Use the guide’s anchor properties to create your constraints.
///
/// When you create constraints using `Anchor` APIs, the constraints are activated
/// automatically and the target view has `translatesAutoresizingMaskIntoConstraints`
/// set to `false`. If you want to activate all the constraints at the same or
/// create them without activation, use `Constraints` type.
public struct Anchor<Type, Axis> { // type and axis are phantom types
    let item: LayoutItem
    let attribute: NSLayoutConstraint.Attribute
    let offset: CGFloat
    let multiplier: CGFloat

    init(_ item: LayoutItem, _ attribute: NSLayoutConstraint.Attribute, offset: CGFloat = 0, multiplier: CGFloat = 1) {
        self.item = item; self.attribute = attribute; self.offset = offset; self.multiplier = multiplier
    }

    /// Returns a new anchor offset by a given amount.
    ///
    /// - note: Consider using a convenience operator instead: `view.anchors.top + 10`.
    public func offsetting(by offset: CGFloat) -> Anchor {
        Anchor(item, attribute, offset: self.offset + offset, multiplier: self.multiplier)
    }

    /// Returns a new anchor with a given multiplier.
    ///
    /// - note: Consider using a convenience operator instead: `view.anchors.height * 2`.
    public func multiplied(by multiplier: CGFloat) -> Anchor {
        Anchor(item, attribute, offset: self.offset * multiplier, multiplier: self.multiplier * multiplier)
    }
}

public func + <Type, Axis>(anchor: Anchor<Type, Axis>, offset: CGFloat) -> Anchor<Type, Axis> {
    anchor.offsetting(by: offset)
}

public func - <Type, Axis>(anchor: Anchor<Type, Axis>, offset: CGFloat) -> Anchor<Type, Axis> {
    anchor.offsetting(by: -offset)
}

public func * <Type, Axis>(anchor: Anchor<Type, Axis>, multiplier: CGFloat) -> Anchor<Type, Axis> {
    anchor.multiplied(by: multiplier)
}

// MARK: - Anchors (AnchorType.Alignment)
public extension Anchor where Type: AnchorType.Alignment {
    /// Adds a constraint that defines the anchors' attributes as equal to each other.
    @discardableResult func equal<Type: AnchorType.Alignment>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .equal)
    }

    @discardableResult func greaterThanOrEqual<Type: AnchorType.Alignment>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .greaterThanOrEqual)
    }

    @discardableResult func lessThanOrEqual<Type: AnchorType.Alignment>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .lessThanOrEqual)
    }
}

// MARK: - Anchors (AnchorType.Dimension)
public extension Anchor where Type: AnchorType.Dimension {
    /// Adds a constraint that defines the anchors' attributes as equal to each other.
    @discardableResult func equal<Type: AnchorType.Dimension, Axis>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .equal)
    }

    @discardableResult func greaterThanOrEqual<Type: AnchorType.Dimension, Axis>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .greaterThanOrEqual)
    }

    @discardableResult func lessThanOrEqual<Type: AnchorType.Dimension, Axis>(
        _ anchor: Anchor<Type, Axis>,
        constant: CGFloat = 0
    ) -> NSLayoutConstraint {
        Constraints.add(self, anchor, constant: constant, relation: .lessThanOrEqual)
    }
}

// MARK: - Anchors (AnchorType.Dimension)
public extension Anchor where Type: AnchorType.Dimension {
    @discardableResult func equal(_ constant: CGFloat) -> NSLayoutConstraint {
        Constraints.add(item: item, attribute: attribute, relatedBy: .equal, constant: constant)
    }

    @discardableResult func greaterThanOrEqual(_ constant: CGFloat) -> NSLayoutConstraint {
        Constraints.add(item: item, attribute: attribute, relatedBy: .greaterThanOrEqual, constant: constant)
    }

    @discardableResult func lessThanOrEqual(_ constant: CGFloat) -> NSLayoutConstraint {
        Constraints.add(item: item, attribute: attribute, relatedBy: .lessThanOrEqual, constant: constant)
    }

    /// Clamps the dimension of a view to the given limiting range.
    @discardableResult func clamp(to limits: ClosedRange<CGFloat>) -> [NSLayoutConstraint] {
        [greaterThanOrEqual(limits.lowerBound), lessThanOrEqual(limits.upperBound)]
    }
}

// MARK: - Anchors (AnchorType.Edge)
public extension Anchor where Type: AnchorType.Edge {
    /// Pins the edge to the respected edges of the given container.
    @discardableResult func pin(to container: LayoutItem? = nil, inset: CGFloat = 0) -> NSLayoutConstraint {
        let isInverted = [.trailing, .right, .bottom].contains(attribute)
        return Constraints.add(
            self,
            toItem: container ?? item.superview!,
            attribute: attribute,
            constant: (isInverted ? -inset : inset)
        )
    }

    /// Adds spacing between the current anchors.
    @discardableResult func spacing<Type: AnchorType.Edge>(
        _ spacing: CGFloat,
        to anchor: Anchor<Type, Axis>,
        relation: NSLayoutConstraint.Relation = .equal
    ) -> NSLayoutConstraint {
        let isInverted = (attribute == .bottom && anchor.attribute == .top) ||
            (attribute == .right && anchor.attribute == .left) ||
            (attribute == .trailing && anchor.attribute == .leading)
        return Constraints.add(
            self,
            anchor,
            constant: isInverted ? -spacing : spacing,
            relation: isInverted ? relation.inverted : relation
        )
    }
}

// MARK: - Anchors (AnchorType.Center)
public extension Anchor where Type: AnchorType.Center {
    /// Aligns the axis with a superview axis.
    @discardableResult func align(offset: CGFloat = 0) -> NSLayoutConstraint {
        Constraints.add(self, toItem: item.superview!, attribute: attribute, constant: offset)
    }
}
