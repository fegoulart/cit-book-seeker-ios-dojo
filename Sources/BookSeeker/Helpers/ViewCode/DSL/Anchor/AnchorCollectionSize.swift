import UIKit

public struct AnchorCollectionSize {
    let width: Anchor<AnchorType.Dimension, AnchorAxis.Horizontal>
    let height: Anchor<AnchorType.Dimension, AnchorAxis.Vertical>

    // MARK: Core API
    /// Set the size of item.
    @discardableResult public func equal(_ size: CGSize) -> [NSLayoutConstraint] {
        [width.equal(size.width), height.equal(size.height)]
    }

    /// Set the size of item.
    @discardableResult public func greaterThanOrEqul(_ size: CGSize) -> [NSLayoutConstraint] {
        [width.greaterThanOrEqual(size.width), height.greaterThanOrEqual(size.height)]
    }

    /// Set the size of item.
    @discardableResult public func lessThanOrEqual(_ size: CGSize) -> [NSLayoutConstraint] {
        [width.lessThanOrEqual(size.width), height.lessThanOrEqual(size.height)]
    }

    /// Makes the size of the item equal to the size of the other item.
    @discardableResult public func equal<Item: LayoutItem>(
        _ item: Item,
        insets: CGSize = .zero,
        multiplier: CGFloat = 1
    ) -> [NSLayoutConstraint] {
        [
            width.equal(
                item.anchors.width * multiplier - insets.width
            ),
            height.equal(
                item.anchors.height * multiplier - insets.height
            )
        ]
    }

    @discardableResult public func greaterThanOrEqual<Item: LayoutItem>(
        _ item: Item,
        insets: CGSize = .zero,
        multiplier: CGFloat = 1
    ) -> [NSLayoutConstraint] {
        [
            width.greaterThanOrEqual(
            item.anchors.width * multiplier - insets.width
        ),
         height.greaterThanOrEqual(
            item.anchors.height * multiplier - insets.height
         )
        ]
    }

    @discardableResult public func lessThanOrEqual<Item: LayoutItem>(
        _ item: Item,
        insets: CGSize = .zero,
        multiplier: CGFloat = 1
    ) -> [NSLayoutConstraint] {
        [
            width.lessThanOrEqual(
                item.anchors.width * multiplier - insets.width
            ),
            height.lessThanOrEqual(
                item.anchors.height * multiplier - insets.height
            )
        ]
    }
}
