import Foundation

// phantom types
public enum AnchorAxis {
    public class Horizontal {}
    public class Vertical {}
}
