import UIKit

public struct AnchorCollectionCenter {
    // swiftlint:disable identifier_name
    let x: Anchor<AnchorType.Center, AnchorAxis.Horizontal>
    let y: Anchor<AnchorType.Center, AnchorAxis.Vertical>
    // swiftlint:enable identifier_name

    // MARK: Core API
    @discardableResult public func equal<Item: LayoutItem>(
        _ item2: Item,
        offset: CGPoint = .zero
    ) -> [NSLayoutConstraint] {
        [x.equal(item2.anchors.centerX, constant: offset.x), y.equal(item2.anchors.centerY, constant: offset.y)]
    }

    @discardableResult public func greaterThanOrEqual<Item: LayoutItem>(
        _ item2: Item,
        offset: CGPoint = .zero
    ) -> [NSLayoutConstraint] {
        [x.greaterThanOrEqual(
            item2.anchors.centerX,
            constant: offset.x
        ),
         y.greaterThanOrEqual(
            item2.anchors.centerY,
            constant: offset.y
         )]
    }

    @discardableResult public func lessThanOrEqual<Item: LayoutItem>(
        _ item2: Item,
        offset: CGPoint = .zero
    ) -> [NSLayoutConstraint] {
        [x.lessThanOrEqual(
            item2.anchors.centerX,
            constant: offset.x
        ),
         y.lessThanOrEqual(
            item2.anchors.centerY,
            constant: offset.y
         )]
    }

    // MARK: Semantic API
    /// Centers the view in the superview.
    @discardableResult public func align() -> [NSLayoutConstraint] {
        [x.align(), y.align()]
    }

    /// Makes the axis equal to the other collection of axis.
    @discardableResult public func align<Item: LayoutItem>(with item: Item) -> [NSLayoutConstraint] {
        [x.equal(item.anchors.centerX), y.equal(item.anchors.centerY)]
    }
}
