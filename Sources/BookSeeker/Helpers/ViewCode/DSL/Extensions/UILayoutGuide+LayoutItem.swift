import UIKit

extension UILayoutGuide: LayoutItem {
    public var superview: UIView? { owningView }
}
