import UIKit

public protocol ViewCoding {
    func buildViewHierarchy()
    func setupConstraints()
    func additionalSetup()
    func setupAccessibility()
    func buildView()
}

public extension ViewCoding {
    func buildView() {
        buildViewHierarchy()
        setupConstraints()
        additionalSetup()
        setupAccessibility()
    }

    func additionalSetup() {}

    func setupAccessibility() {}
}
