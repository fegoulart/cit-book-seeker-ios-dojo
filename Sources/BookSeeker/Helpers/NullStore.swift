//
//  NullStore.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import Foundation
import BookLoader

class NullStore {}

extension NullStore: BookStoreProtocol {
    func deleteCachedBook(searchTerm: String, countryCode: String?, completion: @escaping DeletionCompletion) {

    }

    func insert(
        _ book: [LocalBook],
        searchTerm: String,
        countryCode: String?,
        timestamp: Date,
        completion: @escaping InsertionCompletion
    ) {

    }

    func retrieve(searchTerm: String, countryCode: String?, completion: @escaping RetrievalCompletion) {

    }

    func retrieveSearchTerms(countryCode: String?, completion: @escaping SearchTermRetrievalCompletion) {

    }

}
