//
//  UIImage+Extensions.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import UIKit

extension UIImage {
    func scaled(targetWidth: CGFloat) -> UIImage {
        let targetSize = CGSize(width: targetWidth, height: 1.33 * targetWidth)
        let widthScaleRatio = targetSize.width / self.size.width
        let scaleFactor = widthScaleRatio
        let scaledImageSize = CGSize(
            width: self.size.width * scaleFactor,
            height: self.size.height * scaleFactor
        )

        let renderer = UIGraphicsImageRenderer(size: scaledImageSize)
        let scaledImage = renderer.image { _ in
            self.draw(in: CGRect(origin: .zero, size: scaledImageSize))
        }
        return scaledImage
    }
}
