//
//  BookLoaderCacheDecorator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import BookLoader

public final class BookLoaderCacheDecorator: BookLoaderProtocol {

    private let decoratee: BookLoaderProtocol
    private let cache: BookCacheProtocol

    public init(decoratee: BookLoaderProtocol, cache: BookCacheProtocol) {
        self.decoratee = decoratee
        self.cache = cache
    }

    public func load(
        with term: String,
        countryCode: String?,
        completion: @escaping (BookLoaderProtocol.Result) -> Void
    ) {
        decoratee.load(
            with: term,
            countryCode: countryCode
        ) { [weak self] result in
            completion(result.map { book in
                self?.cache.saveIgnoringResult(
                    book,
                    searchTerm: term,
                    countryCode: countryCode
                )
                return book
            })
        }
    }
}

private extension BookCacheProtocol {
    func saveIgnoringResult(
        _ book: [Book],
        searchTerm: String,
        countryCode: String?
    ) {
        save(book, searchTerm: searchTerm, countryCode: countryCode) { _ in }
    }
}
