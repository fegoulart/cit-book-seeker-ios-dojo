//
//  MainQueueDispatchDecorator.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

import Foundation
import BookLoader

final class MainQueueDispatchDecorator<T> {
    private let decoratee: T

    init(decoratee: T) {
        self.decoratee = decoratee
    }

    func dispatch(completion: @escaping () -> Void) {
        guard Thread.isMainThread else {
            return DispatchQueue.main.async(execute: completion)
        }

        completion()
    }
}

extension MainQueueDispatchDecorator: BookLoaderProtocol where T == BookLoaderProtocol {
    func load(with term: String, countryCode: String?, completion: @escaping (BookLoaderProtocol.Result) -> Void) {
        decoratee.load(with: term, countryCode: countryCode) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}

extension MainQueueDispatchDecorator: PreviousSearchTermLoaderProtocol where T == PreviousSearchTermLoaderProtocol {
    func loadPreviousSearchTerms(
        countryCode: String?,
        completion: @escaping (PreviousSearchTermLoaderProtocol.Result) -> Void
    ) {
        decoratee.loadPreviousSearchTerms(countryCode: countryCode) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}

extension MainQueueDispatchDecorator: BookImageDataLoader where T == BookImageDataLoader {
    func loadImageData(
        from url: URL,
        completion: @escaping (BookImageDataLoader.Result) -> Void
    ) -> BookImageDataLoaderTask {
        decoratee.loadImageData(from: url) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}
