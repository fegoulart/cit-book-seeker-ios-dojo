//
//  BookLoaderWithFallbackComposite.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 18/01/22.
//  Copyright © 2022 Goulart. All rights reserved.
//

import BookLoader

public class BookLoaderWithFallbackComposite: BookLoaderProtocol {

    private let primary: BookLoaderProtocol
    private let fallback: BookLoaderProtocol

    public init(
        primary: BookLoaderProtocol,
        fallback: BookLoaderProtocol
    ) {
        self.primary = primary
        self.fallback = fallback
    }

    public func load(
        with term: String,
        countryCode: String?,
        completion: @escaping (BookLoaderProtocol.Result) -> Void
    ) {
        primary.load(
            with: term,
            countryCode: countryCode
        ) { [weak self] result in
            switch result {
            case .success:
                completion(result)
            case .failure:
                self?.fallback.load(with: term, countryCode: countryCode, completion: completion)
            }
        }
    }
}
