//
//  ViewControllerPreview.swift
//  BookSeeker
//
//  Created by Fernando Luiz Goulart on 15/01/22.
//  Copyright © 2022 Leapi. All rights reserved.
//

#if DEBUG
import SwiftUI

struct ViewControllerPreview: UIViewControllerRepresentable {

    typealias UIViewControllerType = UIViewController
    let viewControllerBuilder: () -> UIViewController

    init(_ viewControllerBuilder: @escaping () -> UIViewController) {
        self.viewControllerBuilder = viewControllerBuilder
    }
    func makeUIViewController(context: Context) -> UIViewController {
        return viewControllerBuilder()
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {

    }
}
#endif
