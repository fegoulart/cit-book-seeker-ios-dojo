# BookSeeker 

## Setup
Run the following command:
```
tuist generate
```

## Architecture
* [Clean Architecture Based](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) 
* MVVM UI pattern
* Separate module (BookLoader) responsible for both Remote and Local Repository with 97% unit testing coverage

### UIKit MVVM Pattern
* ViewModels must be platform agnostic.
* ViewModels should not know about UIKit (or SWIFTUI)
* ViewModels should not know about the Views
* ViewController just as binder (View - ViewModel)

### Some Design Patterns present in project:
* Adapter
* Composite
* Coordinator
* Decorator
* Delegate
* Mapper

### Implementation details
* Networking implemented with URLSession
* Local caching implemented with CoreData (with cache expiration logic) 

## Dependencies
* [tuist](https://tuist.io/) - manages and generates xcodeproj and xcworkspace files 
* [swiftlint](https://github.com/realm/SwiftLint) - static analysis  
* [align DSL](https://github.com/kean/Align) - Viewcoding DSL

## Requirements
* XCode 13
* iOS 15.0 (iPhone)

### Possible improvements:

* Implement Combine for MVVM events
* Unit Test for main module (BookLoader module already has 97% coverage)
* UI Tests
* Automatic build / deploy pipeline (eg: Fastlane) 
* Add an app icon 
* Image caching (only data is being cached)
* Dark mode
* Implement accessibility features
* Ipad / MacOS / WatchOs version
